#!/bin/bash

function gen() {
    echo "data, WER, ins, del, sub"
    awk '{ printf "%s, %s, %s, %s, %s\n", $14, $2, $7, $9, $11; }' $1
} > $2

for i in test_*_res; do
    gen $i ${i}.csv
done
