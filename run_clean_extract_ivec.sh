#!/bin/bash
# Copyright 2013   Daniel Povey
#           2014   David Snyder
#           2015   Ted Yin
# Apache 2.0.
#
# See README.txt for more info on data required.
# Results (EERs) are inline in comments below.

# This example script is still a bit of a mess, and needs to be
# cleaned up, but it shows you all the basic ingredients.

. cmd.sh
. path.sh

set -e
mfccdir=`pwd`/mfcc
train_set=train_si84_clean

false && {
# Get male and female subsets of training data.
    grep -w m data/$train_set/spk2gender | awk '{print $1}' > foo;
    utils/subset_data_dir.sh --spk-list foo data/$train_set data/${train_set}_male
        grep -w f data/$train_set/spk2gender | awk '{print $1}' > foo;
    utils/subset_data_dir.sh --spk-list foo data/$train_set data/${train_set}_female
        rm foo
}

## Get smaller subsets of training data for faster training.
false && {
    utils/subset_data_dir.sh data/$train_set 4000 data/${train_set}_4k
    #utils/subset_data_dir.sh data/$train_set 8000 data/${train_set}_8k
    #utils/subset_data_dir.sh data/${train_set}_male 8000 data/${train_set}_male_8k
    #utils/subset_data_dir.sh data/${train_set}_female 8000 data/${train_set}_female_8k
}

# Split test data by gender
false && {
    for i in {airport,babble,car,clean,restaurant,street,train}_{wv1,wv2} dev_0330; do
        grep -w m data/$i/spk2gender | awk '{print $1}' > foo;
    utils/subset_data_dir.sh --spk-list foo data/$i data/${i}_male
        grep -w f data/$i/spk2gender | awk '{print $1}' > foo;
    utils/subset_data_dir.sh --spk-list foo data/$i data/${i}_female
        rm foo
        utils/fix_data_dir.sh data/${i}_male
        utils/fix_data_dir.sh data/${i}_female
        done
}

# Split test data by gender
false && {
    for i in dev_0330; do
        grep -w m data/$i/spk2gender | awk '{print $1}' > foo;
    utils/subset_data_dir.sh --spk-list foo data/$i data/${i}_male
        grep -w f data/$i/spk2gender | awk '{print $1}' > foo;
    utils/subset_data_dir.sh --spk-list foo data/$i data/${i}_female
        rm foo
        utils/fix_data_dir.sh data/${i}_male
        utils/fix_data_dir.sh data/${i}_female
        done
}

false && {
# The recipe currently uses delta-window=3 and delta-order=2. However
# the accuracy is almost as good using delta-window=4 and delta-order=1
# and could be faster due to lower dimensional features.  Alternative
# delta options (e.g., --delta-window 4 --delta-order 1) can be provided to
# sid/train_diag_ubm.sh.  The options will be propagated to the other scripts.
    ./train_diag_ubm.sh --nj 30 --cmd "$train_cmd" data/${train_set}_4k 2048 \
        exp/diag_ubm_2048_clean

    ./train_full_ubm.sh --nj 30 --cmd "$train_cmd" data/${train_set} \
        exp/diag_ubm_2048_clean exp/full_ubm_2048_clean
}

false && {
# Get male and female versions of the UBM in one pass; make sure not to remove
# any Gaussians due to low counts (so they stay matched).  This will be 
# more convenient for gender-id.
    ./train_full_ubm.sh --nj 30 --remove-low-count-gaussians false \
        --num-iters 1 --cmd "$train_cmd" \
        data/${train_set}_male exp/full_ubm_2048_clean exp/full_ubm_2048_clean_male &

    ./train_full_ubm.sh --nj 30 --remove-low-count-gaussians false \
        --num-iters 1 --cmd "$train_cmd" \
        data/${train_set}_female exp/full_ubm_2048_clean exp/full_ubm_2048_clean_female &
    wait
}

ivector_dim=100

false && {

# Train the iVector extractor for male speakers.
    ./train_ivector_extractor.sh --cmd "$train_cmd -l mem_free=8G,ram_free=8G" \
        --num-iters 5 --ivector-dim $ivector_dim exp/full_ubm_2048_clean_male/final.ubm data/${train_set}_male \
        exp/extractor_2048_clean_male_$ivector_dim

# The same for female speakers.
    ./train_ivector_extractor.sh --cmd "$train_cmd -l mem_free=8G,ram_free=8G" \
        --num-iters 5 --ivector-dim $ivector_dim exp/full_ubm_2048_clean_female/final.ubm data/${train_set}_female \
        exp/extractor_2048_clean_female_$ivector_dim
}

false && {
# The script below demonstrates the gender-id script.  We don't really use
# it for anything here, because the SRE 2008 data is already split up by
# gender and gender identification is not required for the eval.
# It prints out the error rate based on the info in the spk2gender file;
# see exp/gender_id_fisher/error_rate where it is also printed.
    ./gender_id.sh --cmd "$train_cmd" --nj 20 exp/full_ubm_2048_clean{,_male,_female} \
        data/$train_set exp/gender_id_${train_set}
# Gender-id error rate is 3.41%
}

# Extract the iVectors for training data
false && {
    ./extract_ivectors.sh --cmd "$train_cmd -l mem_free=3G,ram_free=3G" --nj 20 \
        exp/extractor_2048_clean_male_$ivector_dim data/${train_set}_male exp/ivectors_${train_set}_male_$ivector_dim

    ./extract_ivectors.sh --cmd "$train_cmd -l mem_free=3G,ram_free=3G" --nj 20 \
        exp/extractor_2048_clean_female_$ivector_dim data/${train_set}_female exp/ivectors_${train_set}_female_$ivector_dim
}

# Extract the iVectors for test data
false && {
    for i in {airport,babble,car,clean,restaurant,street,train}_{wv1,wv2}; do
        ./extract_ivectors.sh --cmd "$train_cmd -l mem_free=3G,ram_free=3G" --nj 1 \
            exp/extractor_2048_clean_male_$ivector_dim data/${i}_male exp/ivectors_${i}_male_$ivector_dim
        ./extract_ivectors.sh --cmd "$train_cmd -l mem_free=3G,ram_free=3G" --nj 1 \
            exp/extractor_2048_clean_female_$ivector_dim data/${i}_female exp/ivectors_${i}_female_$ivector_dim
    done
}

# Extract the iVectors for DNN CV data
false && {
    for i in dev_0330; do
        ./extract_ivectors.sh --cmd "$train_cmd -l mem_free=3G,ram_free=3G" --nj 1 \
            exp/extractor_2048_clean_male_$ivector_dim data/${i}_male exp/ivectors_${i}_male_$ivector_dim
        ./extract_ivectors.sh --cmd "$train_cmd -l mem_free=3G,ram_free=3G" --nj 1 \
            exp/extractor_2048_clean_female_$ivector_dim data/${i}_female exp/ivectors_${i}_female_$ivector_dim
    done
}
