#!/bin/bash
ubm_name="multi"
ivector_dim=100
vaddir="$(pwd)/mfcc"
. run_extract_ivec_with_vad.sh
for x in clean airport babble car restaurant street train ; do
for wv in wv1 wv2; do
train_set="aurora4_${x}_${wv}"
##sed -i "s=mfcc/vad_aurora4_${x}_${wv}=mfcc/vad_aurora4_clean_${wv}=g" data/$train_set/vad.scp
#sed -i "s=mfcc/vad_aurora4_clean_wv1=mfcc/vad_aurora4_clean_${wv}=g" data/$train_set/vad.scp
#compute_vad
split_by_gender "$train_set" #prepare_data
#train_ubm
#echo "train by gender"
#train_ubm_by_gender
#train_ivector_extractor
extract_ivectors "$train_set" "$train_set" "" 2
done
done
