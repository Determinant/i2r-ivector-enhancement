#!/bin/bash
set -x
testsets="clean_wv1 clean_wv2 airport_wv1 airport_wv2 babble_wv1 babble_wv2 car_wv1 car_wv2 restaurant_wv1 restaurant_wv2 street_wv1 street_wv2 train_wv1 train_wv2"
trainsets="train_si84_multi dev_0330"
[[ -z "$nn_file" ]]
[[ -z "$nn_param" ]]
[[ -z "$ivector_dim" ]] && ivector_dim=100

function extract_aurora4_train_uttn {
    name="$1"
    file_prefix="aurora4_train_ivectors/aurora4_utt_ivector_${i}${name}${nn_suffix}"
    cat exp/ivectors_aurora4_${i}${name}_{,fe}male_${ivector_dim}/ivector.scp | copy-vector scp:- ark,t:- | ivector-normalize-length ark:- "ark,scp,t:${file_prefix}_norm.ark,${file_prefix}_norm.scp"
}

function extract_aurora4_test_uttn {
    name="$1"
    file_prefix="aurora4_test_ivectors/aurora4_utt_ivector_test_${i}${name}${nn_suffix}"
    cat exp/ivectors_aurora4_${i}${name}_{,fe}male_${ivector_dim}/ivector.scp | copy-vector scp:- ark,t:- | ivector-normalize-length ark:- "ark,scp,t:${file_prefix}_norm.ark,${file_prefix}_norm.scp"
}

function extract_aurora4_train {
    for i in $trainsets; do
        recover_file="aurora4_utt_ivector_${i}${name}${nn_suffix}_recover"
        file_prefix="aurora4_train_ivectors/aurora4_utt_ivector_${i}${name}${nn_suffix}"
        extract_aurora4_train_uttn $name
        ../merge-vector-to-matrix --fpu=512  --recover_map="ark:$recover_file" --utt2spk="ark:data/aurora4_${i}/utt2spk" --utt2noise="ark:data/aurora4_${i}/utt2spk" "scp:${file_prefix}_norm.scp" ark,scp,t:"${file_prefix}_norm_bulk.ark,${file_prefix}_norm_bulk.scp"
        [[ -z "$dump_clean_spkr" ]] || { nerv mtl_extractor.lua "$nn_file" "$nn_param" "ark:copy-feats scp:${file_prefix}_norm_bulk.scp ark:- |" "$recover_file" 0
        ivector-mean < mtl_dumped ark:data/aurora4_${i}/spk2utt ark:- "ark,scp,t:${file_prefix}_dumped_clean_spkr.ark,${file_prefix}_dumped_clean_spkr.scp"; }
        [[ -z "$dump_noisen" ]] || { nerv mtl_extractor.lua "$nn_file" "$nn_param" "ark:copy-feats scp:${file_prefix}_norm_bulk.scp ark:- |" "$recover_file" 1
        ivector-normalize-length < mtl_dumped ark:- "ark,scp,t:${file_prefix}_dumped_noisen.ark,${file_prefix}_dumped_noisen.scp"; }
        [[ -z "$dump_btnkn" ]] || { nerv mtl_extractor.lua "$nn_file" "$nn_param" "ark:copy-feats scp:${file_prefix}_norm_bulk.scp ark:- |" "$recover_file" 2 
        ivector-normalize-length < mtl_dumped ark:- "ark,scp,t:${file_prefix}_dumped_btnkn.ark,${file_prefix}_dumped_btnkn.scp"; }
    done
}

function extract_aurora4_test {
    for i in $testsets; do
        recover_file="aurora4_utt_ivector_test_${i}${name}${nn_suffix}_recover"
        file_prefix="aurora4_test_ivectors/aurora4_utt_ivector_test_${i}${name}${nn_suffix}"
        extract_aurora4_test_uttn $name
        ../merge-vector-to-matrix --fpu=512  --recover_map="ark:$recover_file" --utt2spk="ark:../test_utt2spk/test_utt2spk_${i}" --utt2noise="ark:../test_utt2spk/test_utt2spk_${i}" "scp:${file_prefix}_norm.scp" ark,scp,t:"${file_prefix}_norm_bulk.ark,${file_prefix}_norm_bulk.scp"
        [[ -z "$dump_clean_spkr" ]] || { nerv mtl_extractor.lua "$nn_file" "$nn_param" "ark:copy-feats scp:${file_prefix}_norm_bulk.scp ark:- |" "$recover_file" 0
        ivector-mean < mtl_dumped ark:data/aurora4_${i}/spk2utt ark:- "ark,scp,t:${file_prefix}_dumped_clean_spkr.ark,${file_prefix}_dumped_clean_spkr.scp"; }
        [[ -z "$dump_noisen" ]] || { nerv mtl_extractor.lua "$nn_file" "$nn_param" "ark:copy-feats scp:${file_prefix}_norm_bulk.scp ark:- |" "$recover_file" 1 
        ivector-normalize-length < mtl_dumped ark:- "ark,scp,t:${file_prefix}_dumped_noisen.ark,${file_prefix}_dumped_noisen.scp"; }
        [[ -z "$dump_btnkn" ]] || { nerv mtl_extractor.lua "$nn_file" "$nn_param" "ark:copy-feats scp:${file_prefix}_norm_bulk.scp ark:- |" "$recover_file" 2 
        ivector-normalize-length < mtl_dumped ark:- "ark,scp,t:${file_prefix}_dumped_btnkn.ark,${file_prefix}_dumped_btnkn.scp"; }
    done
}
