require 'kaldi_io'
gconf = {lrate = 0.08, wcost = 1e-6, momentum = 0.9,
        cumat_type = nerv.CuMatrixFloat,
        mmat_type = nerv.MMatrixFloat,
        tr_scp = "ark:copy-feats scp:data/utt_ivector_noisy2n_100_bulk/feats_head.scp ark:- |",
--        cv_scp = "ark:copy-feats scp:data/utt_ivector_noisy2n_100_bulk/feats_tail.scp ark:- |",
--        tr_scp = "ark:copy-feats scp:data/utt_ivector_noisy2n_100_bulk/feats.scp ark:- |",
        cv_scp = "ark:copy-feats scp:data/utt_ivector_noisyn_100_cv_bulk/feats.scp ark:- |",

        --initialized_param = {"mtl_rbm2n_1024_768_100_768_1024.nerv", "noise_ivector.nerv", "spkr_ivector.nerv", "feature_transform2n.nerv"},
        initialized_param = {"test2n-5p/mtl_rbm2n_1024_768_100_768_1024_20150828165507_iter_31_lr0.002239_tr0.323_cv0.401.nerv", "feature_transform2n.nerv"},
        debug = false}

function make_layer_repo(param_repo)
    local layer_repo = nerv.LayerRepo(
    {
        -- global transf
        ["nerv.BiasLayer"] =
        {
            blayer1 = {{bias = "bias0"}, {dim_in = {100}, dim_out = {100}}}
        },
        ["nerv.WindowLayer"] =
        {
            wlayer1 = {{window = "window0"}, {dim_in = {100}, dim_out = {100}}}
        },
        -- biased linearity
        ["nerv.AffineLayer"] =
        {
            affine0 = {{ltp = "affine0_ltp", bp = "affine0_bp"},
            {dim_in = {100}, dim_out = {1024}}},
            affine1 = {{ltp = "affine1_ltp", bp = "affine1_bp"},
            {dim_in = {1024}, dim_out = {768}}},
            affine2 = {{ltp = "affine2_ltp", bp = "affine2_bp"},
            {dim_in = {768}, dim_out = {100}}},
            affine3 = {{ltp = "affine3_ltp", bp = "affine3_bp"},
            {dim_in = {100}, dim_out = {768}}},
            affine4 = {{ltp = "affine4_ltp", bp = "affine4_bp"},
            {dim_in = {768}, dim_out = {1024}}},

            affine10 = {{ltp = "affine10_ltp", bp = "affine10_bp"},
            {dim_in = {1024}, dim_out = {50}}},
            affine20 = {{ltp = "affine20_ltp", bp = "affine20_bp"},
            {dim_in = {1024}, dim_out = {10}}}
        },
        ["nerv.SigmoidLayer"] =
        {
            sigmoid0 = {{}, {dim_in = {1024}, dim_out = {1024}}},
            sigmoid1 = {{}, {dim_in = {768}, dim_out = {768}}},
            --sigmoid2 = {{}, {dim_in = {100}, dim_out = {100}}},
            sigmoid3 = {{}, {dim_in = {768}, dim_out = {768}}},
            sigmoid4 = {{}, {dim_in = {1024}, dim_out = {1024}}}
        },
        ["nerv.MSELayer"] =
        {
            mse_spkr_clean = {{}, {dim_in = {50, 50}, dim_out = {1}}},
            mse_pure_noise = {{}, {dim_in = {10, 10}, dim_out = {1}}}
        },
        ["nerv.CombinerLayer"] =
        {
            splitter = {{}, {dim_in = {1024}, dim_out = {1024, 1024}, lambda = {1}}},
            final_crit = {{}, {dim_in = {1, 1}, dim_out = {1}, lambda = {0.8, 0.2}}}
        }
    }, param_repo, gconf)

    layer_repo:add_layers(
    {
        ["nerv.DAGLayer"] =
        {
            global_transf = {{}, {
                dim_in = {100}, dim_out = {100},
                sub_layers = layer_repo,
                connections = {
                    ["<input>[1]"] = "blayer1[1]",
                    ["blayer1[1]"] = "wlayer1[1]",
                    ["wlayer1[1]"] = "<output>[1]"
                }
            }},
            main = {{}, {
                dim_in = {100}, dim_out = {1024},
                sub_layers = layer_repo,
                connections = {
                    ["<input>[1]"] = "affine0[1]",
                    ["affine0[1]"] = "sigmoid0[1]",
                    ["sigmoid0[1]"] = "affine1[1]",
                    ["affine1[1]"] = "sigmoid1[1]",
                    ["sigmoid1[1]"] = "affine2[1]",
                    ["affine2[1]"] = "affine3[1]",
--                    ["affine2[1]"] = "sigmoid2[1]",
--                    ["sigmoid2[1]"] = "affine3[1]",
                    ["affine3[1]"] = "sigmoid3[1]",
                    ["sigmoid3[1]"] = "affine4[1]",
                    ["affine4[1]"] = "sigmoid4[1]",
                    ["sigmoid4[1]"] = "<output>[1]",
                }
            }}
        }
    }, param_repo, gconf)

    layer_repo:add_layers(
    {
        ["nerv.DAGLayer"] =
        {
            mse_output = {{}, {
                dim_in = {100, 50, 10}, dim_out = {1},
                sub_layers = layer_repo,
                connections = {
                    ["<input>[1]"] = "main[1]",
                    ["main[1]"] = "splitter[1]",

                    ["splitter[1]"] = "affine10[1]",
                    ["affine10[1]"] = "mse_spkr_clean[1]",
                    ["<input>[2]"] = "mse_spkr_clean[2]",

                    ["splitter[2]"] = "affine20[1]",
                    ["affine20[1]"] = "mse_pure_noise[1]",
                    ["<input>[3]"] = "mse_pure_noise[2]",

                    ["mse_spkr_clean[1]"] = "final_crit[1]",
                    ["mse_pure_noise[1]"] = "final_crit[2]",

                    ["final_crit[1]"] = "<output>[1]"
                }
            }}
        }
    }, param_repo, gconf)

    return layer_repo
end

function get_network(layer_repo)
    return layer_repo:get_layer("mse_output")
end

function get_global_transf(layer_repo)
    return layer_repo:get_layer("global_transf")
end

function make_readers(scp_file, layer_repo)
    return {
                {reader = nerv.KaldiReader(gconf,
                    {
                        id = "ivector_uttr_noisy",
                        feature_rspecifier = scp_file,
                        mlfs = {},
                        lookup = {
                            ivector_spkr_clean = {
                                --targets_rspecifier = "ark:copy-feats scp:spk_ivector.scp ark:- |",
                                targets_rspecifier = "ark:copy-feats scp:spk_ivector-mi.scp ark:- |",
                                map_rspecifier = "ark:utt2spk_multi2"
                            },
                            ivector_pure_noise = {
                                targets_rspecifier = "ark:copy-feats scp:data/noise_ivector/feats.scp ark:- |",
                                map_rspecifier = "ark:utt2noise_multi2"
                            }
                        },
                        -- global_transf = layer_repo:get_layer("global_transf")
                    }),
                data = {ivector_uttr_noisy = 100, ivector_spkr_clean = 50, ivector_pure_noise = 10}}
            }
end

function make_buffer(readers)
    return nerv.SGDBuffer(gconf,
        {
            buffer_size = gconf.buffer_size,
            randomize = gconf.randomize,
            readers = readers,
            use_gpu = true
        })
end

function get_input_order()
    return {{id = "ivector_uttr_noisy", global_transf = true},
            {id = "ivector_spkr_clean"},
            {id = "ivector_pure_noise"}}
end

function get_accuracy(layer_repo)
    local mse_spkr_clean = layer_repo:get_layer("mse_spkr_clean")
    local mse_pure_noise = layer_repo:get_layer("mse_pure_noise")
    local frames = mse_spkr_clean.total_frames
    return (mse_spkr_clean.total_mse + mse_pure_noise.total_mse) / frames
end

function print_stat(layer_repo)
    local mse_spkr_clean = layer_repo:get_layer("mse_spkr_clean")
    local mse_pure_noise = layer_repo:get_layer("mse_pure_noise")
    local frames = mse_spkr_clean.total_frames
    nerv.info("*** training stat begin ***")
    nerv.printf("frames:\t\t\t%d\n", frames)
    nerv.printf("spkr_clean err/frm:\t\t%.8f\n", mse_spkr_clean.total_mse / frames)
    nerv.printf("pure_noise err/frm:\t\t%.8f\n", mse_pure_noise.total_mse / frames)
    nerv.info("*** training stat end ***")
end
