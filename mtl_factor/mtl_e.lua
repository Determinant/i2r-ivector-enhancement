function trim2(s)
    return s:match "^%s*(.-)%s*$"
end
function string:split(sSeparator, nMax, bRegexp)
    assert(sSeparator ~= '')
    assert(nMax == nil or nMax >= 1)
    
    local aRecord = {}
    
    if self:len() > 0 then
        local bPlain = not bRegexp
        nMax = nMax or -1
        
        local nField, nStart = 1, 1
        local nFirst,nLast = self:find(sSeparator, nStart, bPlain)
        while nFirst and nMax ~= 0 do
            aRecord[nField] = self:sub(nStart, nFirst-1)
            nField = nField+1
            nStart = nLast+1
            nFirst,nLast = self:find(sSeparator, nStart, bPlain)
            nMax = nMax-1
        end
        aRecord[nField] = self:sub(nStart)
    end
    
    return aRecord
end

dofile(arg[1])
gconf.batch_size = 1
--gconf.batch_size = 256
gconf.buffer_size = 81920

function make_buffer(readers)
    return nerv.SGDBuffer(gconf,
        {
            buffer_size = gconf.buffer_size,
            randomize = gconf.randomize,
            readers = readers,
            use_gpu = true,
            consume = true
        })
end
--scp_file = "ark:copy-feats scp:data/aurora4_utt_ivector_multin_100_bulk/feats.scp ark:- |"
scp_file = "ark:copy-feats scp:data/aurora4_utt_ivector_multin_100/feats.scp ark:- |"
recover_file = "aurora4_utt_ivector_multin_100_recover"
final_param = "test2n60/mtl_rbm2n_1024_768_60_768_1024_20150901134636_iter_2_lr0.080000_tr1.385_cv1.364.nerv"
transf_param = "feature_transform2n.nerv"
spkr_dim = 50
noise_dim = 10

param_repo = nerv.ParamRepo()
param_repo:import({final_param, transf_param}, nil, gconf)
layer_repo = make_layer_repo(param_repo)
network = layer_repo:get_layer("ex_output")
global_transf = get_global_transf(layer_repo)
input_order = {
    {id = "ivector_uttr_noisy", global_transf = true}
}
readers = make_ex_readers(scp_file, layer_repo)
reader = readers[1].reader
network:init(gconf.batch_size)
gconf.cnt = 0
recover_order = {}
for line in io.lines(recover_file) do
    local splitted = string.split(trim2(line), " ")
    local utter_key = splitted[1]
    local utter_list = {}
    for i = 4, #splitted do
        table.insert(recover_order, splitted[i])
    end
end

cnt = 1
previous_batch_size = gconf.batch_size
for data in reader.get_data, reader do
    local utter = gconf.cumat_type.new_from_host(data['ivector_uttr_noisy'])
    local input = {}
    local transformed
    transformed = nerv.speech_utils.global_transf(utter,
                            global_transf,
                            gconf.frm_ext or 0,
                            gconf.frm_trim or 0,
                            gconf)
    table.insert(input, transformed)
    local batch_size = input[1]:nrow()
    if batch_size ~= previous_batch_size then
        network:init(batch_size)
        previous_batch_size = batch_size
    end
    local output = {nerv.CuMatrixFloat(batch_size, spkr_dim),
                    nerv.CuMatrixFloat(batch_size, noise_dim)}
    local err_input = {nerv.CuMatrixFloat(batch_size, 1)}
    err_input[1]:fill(1)
    local err_output = {input[1]:create()}
    network:propagate(input, output)
--    print(network:get_intermediate("mse_spkr_clean", nerv.DAGLayer.PORT_TYPES.INPUT)[1])
    local pred = output[1]
    for i = 0, pred:nrow() - 1 do
        local frame = pred[i]
        local d = {reader.feat_repo:key() .. " [ "}
--        local d = {recover_order[cnt] .. " [ "}
        for j = 0, frame:ncol() - 1 do
            table.insert(d, string.format("%.8f ", frame[j]))
        end
        table.insert(d, "]")
        print(table.concat(d))
        cnt = cnt + 1
    end
end
print(#recover_order, cnt - 1)
