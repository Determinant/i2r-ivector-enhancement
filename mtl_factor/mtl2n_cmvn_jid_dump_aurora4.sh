#!/bin/bash
. mtl_dump_aurora4.sh
nn_param="test2n60cmvn_jid/mtl_rbm2n_cmvn_1024_768_60_768_1024_20150914135903_iter_50_lr0.000376_tr0.213_cv1.656.nerv"
nn_file="mtl2n_cmvn_jid_1024_768_60_768_1024.lua"
name="_multi_cmvnp0.1"
nn_suffix="_jid"
dump_btnkn=true
extract_aurora4_train
extract_aurora4_test
