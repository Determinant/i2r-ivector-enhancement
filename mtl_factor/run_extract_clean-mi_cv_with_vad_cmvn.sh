#!/bin/bash
ubm_name="multi_cmvn"
ivector_dim=50
vaddir="$(pwd)/mfcc"
train_set="train_si284_multi"
data_name="dev_dt_20_clean-mi"
posterior_scale=0.1
. run_extract_ivec_with_vad_cmvn.sh
#compute_vad
#prepare_data
#train_ubm
#echo "train by gender"
#train_ubm_by_gender
#train_ivector_extractor "p$posterior_scale"
train_set="dev_dt_20_clean"
split_by_gender "$train_set" #prepare_data
extract_ivectors "$train_set" "$data_name" "p$posterior_scale" 5
