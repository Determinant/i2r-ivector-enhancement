#!/bin/bash
nn_param="test2n25cmvn_u25/mtl_rbm2n_cmvn_u25_1024_768_25_768_1024_20150923102744_iter_28_lr0.000806_tr1.049_cv1.045.nerv"
nn_file="mtl2n_cmvn_u25_1024_768_25_768_1024.lua"
transf_file="feature_transform2n_cmvn_u25.nerv"
name="_multi_cmvnp0.1"
nn_suffix="_mse25_u25"
dump_btnkn=true
ivector_dim=25
. mtl_dump_wsj.sh
extract_wsj_train
extract_wsj_test
