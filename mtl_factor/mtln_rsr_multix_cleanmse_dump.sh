#!/bin/bash
nn_param="testn100_sv_400_cleanmse/mtl_rbmn_multix_sv_400_1024_768_100_768_1024_20151126122321_iter_24_lr0.022395_tr0.450_cv0.493.nerv"
nn_file="mtln_multix_sv_400_cleanmse_1024_768_100_768_1024.lua"
transf_file="feature_transformn_multix_sv_400.nerv"
name="_multi"
nn_suffix="_cleanmse"
#dump_btnkn=true
. mtl_dump_rsr.sh
layer_id=affine30
dump_id=mse
dump_type=1
#extract_rsr_test prepare
extract_rsr_test dump_custom
