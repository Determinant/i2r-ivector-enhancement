#!/bin/bash -e
prefix=dev_dt_20
cond=(clean mal gym mes res str sup tax tel)
true && for f in feats.scp utt2spk; do
    for ((i=0;i<${#cond[@]};i++)); do
        awk '{print $1 '$i', $2}' "../${prefix}_${cond[$i]}"/"$f"
    done > "$f"
done
true && for ((i=0;i<${#cond[@]};i++)); do
    awk '{print $1 '$i', $2}' "../${prefix}_clean"/vad.scp
done > vad.scp
true && for ((i=0;i<${#cond[@]};i++)); do
        awk '{print $1 '$i', "'"${cond[$i]}"'"}' "../${prefix}_${cond[$i]}"/utt2spk
done > utt2noise
