#!/bin/bash -e
cond=(clean mal gym mes res str sup tax tel)
false && for f in feats.scp utt2spk; do
    for ((i=0;i<${#cond[@]};i++)); do
        awk '{print $1 '$i', $2}' "../train_si284_${cond[$i]}"/"$f"
    done > "$f"
done
false && for ((i=0;i<${#cond[@]};i++)); do
    awk '{print $1 '$i', $2}' "../train_si284_clean"/vad.scp
done > vad.scp
false && for ((i=0;i<${#cond[@]};i++)); do
        awk '{print $1 '$i', "'"${cond[$i]}"'"}' "../train_si284_${cond[$i]}"/utt2spk
done > utt2noise
false && for ((i=0;i<${#cond[@]};i++)); do
        echo >&2 ${cond[$i]}
        awk '{print $1 '$i', $2 "_'"${cond[$i]}"'"}' "../train_si284_${cond[$i]}"/utt2spk
done > utt2cmvn
true && for ((i=0;i<${#cond[@]};i++)); do
        echo >&2 ${cond[$i]}
        awk '{print $1 "_'"${cond[$i]}"'", $2}' "../train_si284_${cond[$i]}"/spk2gender
done > cmvn2gender
