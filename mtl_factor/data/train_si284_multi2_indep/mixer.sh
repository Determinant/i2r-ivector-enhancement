#!/bin/bash -e
prefix=train_si284
cond=()
cond_tag=()
cond_id=()
let j=1
for i in mal gym mes res str sup tax tel; do 
    for snr in {5..19..2}; do
        cond+=("${i}_${snr}dB")
        cond_tag+=(${i})
        cond_id+=($j)
        let j=$j+1
    done
done
echo ${cond[@]}
echo ${cond_tag[@]}
echo ${cond_id[@]}
false && for f in feats.scp utt2spk; do
    for ((i=0;i<${#cond[@]};i++)); do
        awk '{printf "%s%02d %s\n", $1, '${cond_id[$i]}', $2}' "../${prefix}_${cond[$i]}"/"$f"
    done | sort > "$f"
done
false && for ((i=0;i<${#cond[@]};i++)); do
    awk '{printf "%s%02d %s\n", $1, '${cond_id[$i]}', $2}' "../${prefix}_clean"/vad.scp
done | sort > vad.scp
false && for ((i=0;i<${#cond[@]};i++)); do
        awk '{printf "%s%02d %s\n", $1, '${cond_id[$i]}', "'"${cond_tag[$i]}"'"}' "../${prefix}_${cond[$i]}"/utt2spk
done | sort > utt2noise

false && for ((i=0;i<${#cond[@]};i++)); do
        echo >&2 ${cond[$i]}
        awk '{printf "%s%02d %s_%s\n", $1, '${cond_id[$i]}', $2, "'${cond[$i]}'"}' "../${prefix}_${cond[$i]}"/utt2spk
done | sort > utt2cmvn

true && for ((i=0;i<${#cond[@]};i++)); do
        echo >&2 ${cond[$i]}
        awk '{printf "%s_%s %s\n", $1, "'${cond[$i]}'", $2}' "../${prefix}_${cond[$i]}"/spk2gender
done | sort > cmvn2gender
