#!/bin/bash
../merge-vector-to-matrix --fpu=512 \
                            --recover_map=ark:utt_ivector_rsr_clean_400_recover \
                            --utt2spk=ark:data/utt_ivector_rsr_clean_400_bulk/utt2spk \
                            --utt2noise=ark:data/utt_ivector_rsr_clean_400_bulk/utt2spk \
    'ark:ivector-normalize-length ark:/home/stuymf/rsr2015/scripts/train_iv.ark ark:- |' \
    ark,scp:data/utt_ivector_rsr_clean_400_bulk/feats.ark,data/utt_ivector_rsr_clean_400_bulk/feats.scp
