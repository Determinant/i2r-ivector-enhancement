#!/bin/bash
ubm_name="clean_cmvn_indep"
ivector_dim=25
vaddir="$(pwd)/mfcc"
posterior_scale=0.1
indep=true
. run_extract_ivec_with_vad_cmvn.sh
for x in test_eval9{2,3} test_dev93; do
train_set="wsj_${x}_5k_mfcc_uttn_indep"
awk '{print $1, $1;}' data/$train_set/utt2spk > data/$train_set/utt2utt
./compute_utt_cmvn_stats.sh data/$train_set exp/make_mfcc/$train_set mfcc/
#compute_vad
split_by_gender "$train_set" #prepare_data
#train_ubm
#echo "train by gender"
#train_ubm_by_gender
#train_ivector_extractor
extract_ivectors "$train_set" "$train_set" "" 2
done
