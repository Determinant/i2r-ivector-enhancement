#!/bin/bash
. mtl_dump_aurora4.sh
nn_param="test2n60cmvn_id/mtl_rbm2n_cmvn_1024_768_60_768_1024_20150914172613_iter_36_lr0.000226_tr0.193_cv0.420.nerv"
nn_file="mtl2n_cmvn_id_1024_768_60_768_1024.lua"
name="_multi_cmvnp0.1"
nn_suffix="_id"
dump_btnkn=true
extract_aurora4_train
extract_aurora4_test
