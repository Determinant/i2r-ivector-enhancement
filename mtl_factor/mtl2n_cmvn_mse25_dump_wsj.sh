#!/bin/bash
nn_param="test2n25cmvn/mtl_rbm2n_cmvn_1024_768_25_768_1024_20150926124400_iter_26_lr0.002902_tr0.175_cv0.234.nerv"
nn_file="mtl2n_cmvn_1024_768_25_768_1024.lua"
transf_file="feature_transform2n_cmvn.nerv"
name="_multi_cmvnp0.1"
nn_suffix="_mse25"
dump_btnkn=true
. mtl_dump_wsj.sh
extract_wsj_train
extract_wsj_test_5k
