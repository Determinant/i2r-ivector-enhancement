#!/bin/bash
. mtl_dump_wsj.sh
nn_param="test2n25cmvn/mtl_rbm2n_cmvn_1024_768_25_768_1024_20150922171955_iter_34_lr0.000806_tr0.721_cv0.764.nerv"
nn_file="mtl2n_cmvn_1024_768_25_768_1024.lua"
name="_multi_cmvnp0.1"
nn_suffix="_mse25"
dump_btnkn=true
extract_wsj_train
extract_wsj_test
