require 'kaldi_io'
gconf = {lrate = 0.8, wcost = 1e-6, momentum = 0.9,
        cumat_type = nerv.CuMatrixFloat,
        mmat_type = nerv.MMatrixFloat,
        tr_scp = "ark:copy-feats scp:data/utt_ivector_noisy2n_cmvn_indep_100_bulk/feats_head.scp ark:- |",
        cv_scp = "ark:copy-feats scp:data/utt_ivector_noisy2n_cmvn_indep_100_bulk/feats_tail.scp ark:- |",

        initialized_param = {"mtl_rbm2n_cmvn_indep_1024_768_60_768_1024.nerv", "noise_id.nerv", "spkr_id.nerv", "feature_transform2n_cmvn_indep.nerv"},
        debug = false}

function make_layer_repo(param_repo)
    local layer_repo = nerv.LayerRepo(
    {
        -- global transf
        ["nerv.BiasLayer"] =
        {
            blayer1 = {{bias = "bias0"}, {dim_in = {100}, dim_out = {100}}}
        },
        ["nerv.WindowLayer"] =
        {
            wlayer1 = {{window = "window0"}, {dim_in = {100}, dim_out = {100}}}
        },
        -- biased linearity
        ["nerv.AffineLayer"] =
        {
            affine0 = {{ltp = "affine0_ltp", bp = "affine0_bp"},
            {dim_in = {100}, dim_out = {1024}}},
            affine1 = {{ltp = "affine1_ltp", bp = "affine1_bp"},
            {dim_in = {1024}, dim_out = {768}}},
            affine2 = {{ltp = "affine2_ltp", bp = "affine2_bp"},
            {dim_in = {768}, dim_out = {60}}},
            affine3 = {{ltp = "affine3_ltp", bp = "affine3_bp"},
            {dim_in = {60}, dim_out = {768}}},
            affine4 = {{ltp = "affine4_ltp", bp = "affine4_bp"},
            {dim_in = {768}, dim_out = {1024}}},

            affine10 = {{ltp = "affine10_ltp", bp = "affine10_bp"},
            {dim_in = {1024}, dim_out = {283}}},
            affine20 = {{ltp = "affine20_ltp", bp = "affine20_bp"},
            {dim_in = {1024}, dim_out = {8}}}
        },
        ["nerv.SigmoidLayer"] =
        {
            sigmoid0 = {{}, {dim_in = {1024}, dim_out = {1024}}},
            sigmoid1 = {{}, {dim_in = {768}, dim_out = {768}}},
            --sigmoid2 = {{}, {dim_in = {60}, dim_out = {60}}},
            sigmoid3 = {{}, {dim_in = {768}, dim_out = {768}}},
            sigmoid4 = {{}, {dim_in = {1024}, dim_out = {1024}}}
        },
        ["nerv.SoftmaxCELayer"] =
        {
            ce_spkr_clean = {{}, {dim_in = {283, 1}, dim_out = {1}, compressed = true}},
            ce_pure_noise = {{}, {dim_in = {8, 1}, dim_out = {1}, compressed = true}}
        },
        ["nerv.CombinerLayer"] =
        {
            splitter = {{}, {dim_in = {1024}, dim_out = {1024, 1024}, lambda = {1}}},
            final_crit = {{}, {dim_in = {1, 1}, dim_out = {1}, lambda = {0.9, 0.1}}}
        }
    }, param_repo, gconf)

    layer_repo:add_layers(
    {
        ["nerv.DAGLayer"] =
        {
            global_transf = {{}, {
                dim_in = {100}, dim_out = {100},
                sub_layers = layer_repo,
                connections = {
                    ["<input>[1]"] = "blayer1[1]",
                    ["blayer1[1]"] = "wlayer1[1]",
                    ["wlayer1[1]"] = "<output>[1]"
                }
            }},
            main = {{}, {
                dim_in = {100}, dim_out = {1024},
                sub_layers = layer_repo,
                connections = {
                    ["<input>[1]"] = "affine0[1]",
                    ["affine0[1]"] = "sigmoid0[1]",
                    ["sigmoid0[1]"] = "affine1[1]",
                    ["affine1[1]"] = "sigmoid1[1]",
                    ["sigmoid1[1]"] = "affine2[1]",
                    ["affine2[1]"] = "affine3[1]",
--                    ["affine2[1]"] = "sigmoid2[1]",
--                    ["sigmoid2[1]"] = "affine3[1]",
                    ["affine3[1]"] = "sigmoid3[1]",
                    ["sigmoid3[1]"] = "affine4[1]",
                    ["affine4[1]"] = "sigmoid4[1]",
                    ["sigmoid4[1]"] = "<output>[1]",
                }
            }}
        }
    }, param_repo, gconf)

    layer_repo:add_layers(
    {
        ["nerv.DAGLayer"] =
        {
            ce_output = {{}, {
                dim_in = {100, 1, 1}, dim_out = {1},
                sub_layers = layer_repo,
                connections = {
                    ["<input>[1]"] = "main[1]",
                    ["main[1]"] = "splitter[1]",

                    ["splitter[1]"] = "affine10[1]",
                    ["affine10[1]"] = "ce_spkr_clean[1]",
                    ["<input>[2]"] = "ce_spkr_clean[2]",

                    ["splitter[2]"] = "affine20[1]",
                    ["affine20[1]"] = "ce_pure_noise[1]",
                    ["<input>[3]"] = "ce_pure_noise[2]",

                    ["ce_spkr_clean[1]"] = "final_crit[1]",
                    ["ce_pure_noise[1]"] = "final_crit[2]",

                    ["final_crit[1]"] = "<output>[1]"
                }
            }},
            ex_output = {{}, {
                dim_in = {100}, dim_out = {1024},
                sub_layers = layer_repo,
                connections = {
                    ["<input>[1]"] = "main[1]",
                    ["main[1]"] = "<output>[1]",
                }
            }}
        }
    }, param_repo, gconf)

    return layer_repo
end

function get_network(layer_repo)
    return layer_repo:get_layer("ce_output")
end

function get_global_transf(layer_repo)
    return layer_repo:get_layer("global_transf")
end

function make_readers(scp_file, layer_repo)
    return {
                {reader = nerv.KaldiReader(gconf,
                    {
                        id = "ivector_uttr_noisy",
                        feature_rspecifier = scp_file,
                        mlfs = {},
                        lookup = {
                            ivector_spkr_clean = {
                                --targets_rspecifier = "ark:copy-feats scp:spk_ivector.scp ark:- |",
                                targets_rspecifier = "ark:copy-feats ark:spk_id.ark ark:- |",
                                map_rspecifier = "ark:utt2spk_multi2"
                            },
                            ivector_pure_noise = {
                                targets_rspecifier = "ark:copy-feats ark:noise_id.ark ark:- |",
                                map_rspecifier = "ark:utt2noise_multi2"
                            }
                        },
                        -- global_transf = layer_repo:get_layer("global_transf")
                    }),
                data = {ivector_uttr_noisy = 100, ivector_spkr_clean = 1, ivector_pure_noise = 1}}
            }
end

function make_ex_readers(scp_file, layer_repo)
    return {
                {reader = nerv.KaldiReader(gconf,
                    {
                        id = "ivector_uttr_noisy",
                        feature_rspecifier = scp_file,
                    }),
                data = {ivector_uttr_noisy = 100}}
            }
end

function make_buffer(readers)
    return nerv.SGDBuffer(gconf,
        {
            buffer_size = gconf.buffer_size,
            randomize = gconf.randomize,
            readers = readers,
            use_gpu = true
        })
end

function get_input_order()
    return {{id = "ivector_uttr_noisy", global_transf = true},
            {id = "ivector_spkr_clean"},
            {id = "ivector_pure_noise"}}
end

function get_accuracy(layer_repo)
    local ce_spkr_clean = layer_repo:get_layer("ce_spkr_clean")
    local ce_pure_noise = layer_repo:get_layer("ce_pure_noise")
    local frames = ce_spkr_clean.total_frames
    return (ce_spkr_clean.total_ce + ce_pure_noise.total_ce) / frames
end

function print_stat(layer_repo)
    local ce_spkr_clean = layer_repo:get_layer("ce_spkr_clean")
    local ce_pure_noise = layer_repo:get_layer("ce_pure_noise")
    local frames = ce_spkr_clean.total_frames
    nerv.info("*** training stat begin ***")
    nerv.printf("frames:\t\t\t%d\n", frames)
    nerv.printf("spkr_clean err/frm:\t\t%.8f\n", ce_spkr_clean.total_ce / frames)
    nerv.printf("spkr_clean accu:\t\t%.3f%%\n", ce_spkr_clean.total_correct / frames * 100)
    nerv.printf("pure_noise err/frm:\t\t%.8f\n", ce_pure_noise.total_ce / frames)
    nerv.printf("pure_noise accu:\t\t%.3f%%\n", ce_pure_noise.total_correct / frames * 100)
    nerv.info("*** training stat end ***")
end
