#!/bin/bash
ubm_name="multi"
ivector_dim=100
vaddir="$(pwd)/mfcc"

. run_extract_ivec_with_vad.sh
for x in test_eval9{2,3} test_dev93; do
train_set="wsj_${x}_multi"
awk '{print $1, $1;}' data/$train_set/utt2spk > data/$train_set/utt2utt
#compute_vad
split_by_gender "$train_set" #prepare_data
#train_ubm
#echo "train by gender"
#train_ubm_by_gender
#train_ivector_extractor
extract_ivectors "$train_set" "$train_set" "" 2
done
