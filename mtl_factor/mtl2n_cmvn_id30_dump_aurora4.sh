#!/bin/bash
. mtl_dump_aurora4.sh
nn_param="test2n30cmvn_id/mtl_rbm2n_cmvn_1024_768_30_768_1024_20150915224517_iter_36_lr0.000376_tr0.199_cv0.437.nerv"
nn_file="mtl2n_cmvn_id_1024_768_30_768_1024.lua"
name="_multi_cmvnp0.1"
nn_suffix="_id30"
dump_btnkn=true
extract_aurora4_train
extract_aurora4_test
