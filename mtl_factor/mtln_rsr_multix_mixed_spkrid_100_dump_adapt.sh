#!/bin/bash
nn_param="testn100_mixed_sv_400_spkrid_adapt/mtl_rbmn_multix_mixed_sv_400_1024_1024_768_100_768_20160118155324_iter_6_lr0.288000_tr0.001_cv0.014_20160118220348_iter_2_lr0.800000_tr0.046_cv0.017.nerv"
nn_file="mtln_multix_mixed_sv_400_spkrid_1024_1024_768_100_768.lua"
transf_file="feature_transformn_multix_mixed_sv_400.nerv"
name="_multi"
nn_suffix="_mixed_spkrid"
datadir="rsr_test_mixed"
dump_btnkn=true
. mtl_dump_rsr.sh
layer_id=affine3
dump_id=id_100_adapt
dump_type=2
extract_rsr_test prepare
extract_rsr_test dump_custom
