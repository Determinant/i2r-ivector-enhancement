#!/bin/bash

. ./cmd.sh ## You'll want to change cmd.sh to something that will work on your system.
           ## This relates to the queue.

. ./path.sh
# This is a shell script, but it's recommended that you run the commands one by
# one by copying and pasting into the shell.
function join { local IFS="$1"; shift; echo "$*"; }

set -x
nn_name="sv_multix_mixed_sigmoid"
ivector_dim=400
train_set="utt_ivector_rsr_multix_mixed_${ivector_dim}_bulk"
hid_dim=(1024 1024 768 90 768)
hid_activ=(bern bern bern bern bern)
rbm_iter=10
function rbm_pretrain {
    #Now begin train DNN systems on data
    #RBM pretrain
    dir=exp/ivector_multi_${ivector_dim}_dnn$(join _ "${hid_dim[@]}")_${nn_name}_pretrain
    $cuda_cmd $dir/_pretrain_dbn.log \
        ./pretrain_dbn_custom.sh \
        --hid-dim "$(join '_' ${hid_dim[@]})" \
        --hid-activ "$(join '_' ${hid_activ[@]})" \
        --rbm-iter $rbm_iter \
        --splice 0 \
        data/$train_set $dir
}

function make_nnet_proto {
    mlp_proto="$1"
    num_fea="$2"
    num_tgt="$3"
    hid_layers="$4"
    hid_dim="$5"
    utils/nnet/make_nnet_proto.py $proto_opts \
        ${bn_dim:+ --bottleneck-dim=$bn_dim} \
        $num_fea $num_tgt $hid_layers $hid_dim >$mlp_proto || exit 1 
}

function make_nnet_init {
    nnet-initialize --binary=false "$1" "$2"
}

function make_nerv_init {
    mlp_name="$1"
    num_fea="$2"
    num_tgt="$3"
    pcnt="$4"
    mlp_proto="${mlp_name}.proto"
    mlp_init="${mlp_name}.init"
    make_nnet_proto "$mlp_proto" "$num_fea" "$num_tgt" 0 "$num_fea"
    make_nnet_init "$mlp_proto" "$mlp_init"
    ./kaldi_to_nerv < "$mlp_init" "${mlp_name}.nerv" "$pcnt"
}

rbm_pretrain
false && {
    make_nerv_init spkr_ivector ${hid_dim} 50 10
    make_nerv_init noise_ivector ${hid_dim} 10 20
}
false && {
    make_nerv_init spkr_id ${hid_dim} 283 10
    make_nerv_init noise_id ${hid_dim} 8 20
}
#make_nerv_init rsr_sid ${hid_dim} 206 30
