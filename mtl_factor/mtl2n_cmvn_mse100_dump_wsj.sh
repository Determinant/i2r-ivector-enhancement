#!/bin/bash
nn_param="test2n100cmvn/mtl_rbm2n_cmvn_1024_768_100_768_1024_20150925194507_iter_22_lr0.008062_tr0.131_cv0.187.nerv"
nn_file="mtl2n_cmvn_1024_768_100_768_1024.lua"
transf_file="feature_transform2n_cmvn.nerv"
name="_multi_cmvnp0.1"
nn_suffix="_mse100"
dump_btnkn=true
. mtl_dump_wsj.sh
extract_wsj_train
extract_wsj_test_5k
