#!/bin/bash
nn_param="test2n60cmvn_indep_id_0.1/mtl_rbm2n_cmvn_indep_1024_768_60_768_1024_20151015143818_iter_30_lr0.000376_tr0.170_cv0.382.nerv"
nn_file="mtl2n_cmvn_indep_id_1024_768_60_768_1024_0.1.lua"
transf_file="feature_transform2n_cmvn_indep.nerv"
name="_indep_multi_cmvn_indepp0.1"
nn_suffix="_indep_id_0.1"
dump_btnkn=true
. mtl_dump_wsjb_indep.sh
extract_wsj_train
#extract_wsj_test
extract_wsj_test_5k
