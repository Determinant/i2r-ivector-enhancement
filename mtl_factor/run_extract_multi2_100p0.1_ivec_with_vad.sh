#!/bin/bash
ubm_name="multi"
ivector_dim=100
vaddir="$(pwd)/mfcc"
train_set="train_si284_multi2"
posterior_scale=0.1
. run_extract_ivec_with_vad.sh
#compute_vad
#split_by_gender "$train_set" #prepare_data
#train_ubm
#echo "train by gender"
#train_ubm_by_gender
train_ivector_extractor "p$posterior_scale" 20
extract_ivectors "$train_set" "${train_set}" "p$posterior_scale" 48
