require 'kaldi_io'
gconf = {lrate = 0.8, wcost = 1e-6, momentum = 0.9,
        cumat_type = nerv.CuMatrixFloat,
        mmat_type = nerv.MMatrixFloat,
        tr_scp = "ark:copy-feats scp:data/utt_ivector_rsr_multix2_400_bulk/feats_head.scp ark:- |",
        cv_scp = "ark:copy-feats scp:data/utt_ivector_rsr_multix2_400_bulk/feats_tail.scp ark:- |",

        initialized_param = {"mtl_rbmn_multix_sv_400_1024_768_100_768_1024.nerv",
                            "rsr_400_1024.nerv", "noise_id.nerv", "feature_transformn_multix_sv_400.nerv"},
        debug = false}

function make_layer_repo(param_repo)
    local layer_repo = nerv.LayerRepo(
    {
        -- global transf
        ["nerv.BiasLayer"] =
        {
            blayer1 = {{bias = "bias0"}, {dim_in = {400}, dim_out = {400}}}
        },
        ["nerv.WindowLayer"] =
        {
            wlayer1 = {{window = "window0"}, {dim_in = {400}, dim_out = {400}}}
        },
        -- biased linearity
        ["nerv.AffineLayer"] =
        {
            affine0 = {{ltp = "affine0_ltp", bp = "affine0_bp"},
            {dim_in = {400}, dim_out = {1024}}},
            affine1 = {{ltp = "affine1_ltp", bp = "affine1_bp"},
            {dim_in = {1024}, dim_out = {768}}},
            affine2 = {{ltp = "affine2_ltp", bp = "affine2_bp"},
            {dim_in = {768}, dim_out = {100}}},
            affine3 = {{ltp = "affine3_ltp", bp = "affine3_bp"},
            {dim_in = {100}, dim_out = {768}}},
            affine4 = {{ltp = "affine4_ltp", bp = "affine4_bp"},
            {dim_in = {768}, dim_out = {1024}}},

            affine30 = {{ltp = "affine30_ltp", bp = "affine30_bp"},
            {dim_in = {1024}, dim_out = {400}}},
            affine20 = {{ltp = "affine20_ltp", bp = "affine20_bp"},
            {dim_in = {1024}, dim_out = {8}}}
        },
        ["nerv.SigmoidLayer"] =
        {
            sigmoid0 = {{}, {dim_in = {1024}, dim_out = {1024}}},
            sigmoid1 = {{}, {dim_in = {768}, dim_out = {768}}},
            --sigmoid2 = {{}, {dim_in = {100}, dim_out = {100}}},
            sigmoid3 = {{}, {dim_in = {768}, dim_out = {768}}},
            sigmoid4 = {{}, {dim_in = {1024}, dim_out = {1024}}}
        },
        ["nerv.SoftmaxCELayer"] =
        {
            ce_nid = {{}, {dim_in = {8, 1}, dim_out = {1}, compressed = true}}
        },
        ["nerv.MSELayer"] =
        {
            mse_sid  = {{}, {dim_in = {400, 400}, dim_out = {1}}}
        },
        ["nerv.CombinerLayer"] =
        {
            splitter = {{}, {dim_in = {1024}, dim_out = {1024, 1024}, lambda = {1}}},
            final_crit = {{}, {dim_in = {1, 1}, dim_out = {1}, lambda = {0.8, 0.2}}}
        }
    }, param_repo, gconf)

    layer_repo:add_layers(
    {
        ["nerv.DAGLayer"] =
        {
            global_transf = {{}, {
                dim_in = {400}, dim_out = {400},
                sub_layers = layer_repo,
                connections = {
                    ["<input>[1]"] = "blayer1[1]",
                    ["blayer1[1]"] = "wlayer1[1]",
                    ["wlayer1[1]"] = "<output>[1]"
                }
            }},
            main = {{}, {
                dim_in = {400}, dim_out = {1024},
                sub_layers = layer_repo,
                connections = {
                    ["<input>[1]"] = "affine0[1]",
                    ["affine0[1]"] = "sigmoid0[1]",
                    ["sigmoid0[1]"] = "affine1[1]",
                    ["affine1[1]"] = "sigmoid1[1]",
                    ["sigmoid1[1]"] = "affine2[1]",
                    ["affine2[1]"] = "affine3[1]",
--                    ["affine2[1]"] = "sigmoid2[1]",
--                    ["sigmoid2[1]"] = "affine3[1]",
                    ["affine3[1]"] = "sigmoid3[1]",
                    ["sigmoid3[1]"] = "affine4[1]",
                    ["affine4[1]"] = "sigmoid4[1]",
                    ["sigmoid4[1]"] = "<output>[1]",
                }
            }}
        }
    }, param_repo, gconf)

    layer_repo:add_layers(
    {
        ["nerv.DAGLayer"] =
        {
            ce_output = {{}, {
                dim_in = {400, 400, 1}, dim_out = {1},
                sub_layers = layer_repo,
                connections = {
                    ["<input>[1]"] = "main[1]",
                    ["main[1]"] = "splitter[1]",

                    ["splitter[1]"] = "affine30[1]",
                    ["affine30[1]"] = "mse_sid[1]",
                    ["<input>[2]"] = "mse_sid[2]",

                    ["splitter[2]"] = "affine20[1]",
                    ["affine20[1]"] = "ce_nid[1]",
                    ["<input>[3]"] = "ce_nid[2]",

                    ["mse_sid[1]"] = "final_crit[1]",
                    ["ce_nid[1]"] = "final_crit[2]",

                    ["final_crit[1]"] = "<output>[1]"
                }
            }},
            ex_output = {{}, {
                dim_in = {400}, dim_out = {1024},
                sub_layers = layer_repo,
                connections = {
                    ["<input>[1]"] = "main[1]",
                    ["main[1]"] = "<output>[1]",
                }
            }}
        }
    }, param_repo, gconf)

    return layer_repo
end

function get_network(layer_repo)
    return layer_repo:get_layer("ce_output")
end

function get_global_transf(layer_repo)
    return layer_repo:get_layer("global_transf")
end

function make_readers(scp_file, layer_repo)
    return {
                {reader = nerv.KaldiReader(gconf,
                    {
                        id = "ivector_uttr_rsr_multix",
                        feature_rspecifier = scp_file,
                        mlfs = {},
                        lookup = {
                            rsr_sid = {
                                targets_rspecifier = "ark:copy-feats ark:rsr_spkr2_400.ark ark:- |",
                                map_rspecifier = "ark:data/utt_ivector_rsr_multix2_400_bulk/utt2cutt"
                            },
                            rsr_nid = {
                                targets_rspecifier = "ark:copy-feats ark:noise_id.ark ark:- |",
                                map_rspecifier = "ark:data/utt_ivector_rsr_multix2_400_bulk/utt2noise"
                            }
                        },
                        -- global_transf = layer_repo:get_layer("global_transf")
                    }),
                data = {ivector_uttr_rsr_multix = 400, rsr_sid = 400, rsr_nid = 1}}
            }
end

function make_ex_readers(scp_file, layer_repo)
    return {
                {reader = nerv.KaldiReader(gconf,
                    {
                        id = "ivector_uttr_rsr_multix",
                        feature_rspecifier = scp_file,
                    }),
                data = {ivector_uttr_rsr_multix = 400}}
            }
end

function make_buffer(readers)
    return nerv.SGDBuffer(gconf,
        {
            buffer_size = gconf.buffer_size,
            randomize = gconf.randomize,
            readers = readers,
            use_gpu = true
        })
end

function get_input_order()
    return {{id = "ivector_uttr_rsr_multix", global_transf = true},
            {id = "rsr_sid"},
            {id = "rsr_nid"}}
end

function get_accuracy(layer_repo)
    local mse_sid = layer_repo:get_layer("mse_sid")
    local ce_nid = layer_repo:get_layer("ce_nid")
    local frames = mse_sid.total_frames
    return (0.8 * mse_sid.total_mse + 0.2 * ce_nid.total_ce) / frames
end

function print_stat(layer_repo)
    local mse_sid = layer_repo:get_layer("mse_sid")
    local ce_nid = layer_repo:get_layer("ce_nid")
    local frames = mse_sid.total_frames
    nerv.info("*** training stat begin ***")
    nerv.printf("frames:\t\t\t%d\n", frames)
    nerv.printf("spkr_clean err/frm:\t\t%.8f\n", mse_sid.total_mse / frames)
    nerv.printf("pure_noise err/frm:\t\t%.8f\n", ce_nid.total_ce / frames)
    nerv.printf("pure_noise accu:\t\t%.3f%%\n", ce_nid.total_correct / frames * 100)
    nerv.info("*** training stat end ***")
end
