#!/bin/bash
. mtl_dump_aurora4.sh
nn_param="test2n60cmvn/mtl_rbm2n_cmvn_1024_768_60_768_1024_20150910184628_iter_27_lr0.002902_tr0.143_cv0.197.nerv"
nn_file="t2.lua"
name="_multi_cmvnp0.1"
nn_suffix="_mse"
dump_btnkn=true
extract_aurora4_train
extract_aurora4_test
