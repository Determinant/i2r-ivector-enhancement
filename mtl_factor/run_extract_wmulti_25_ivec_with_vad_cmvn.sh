#!/bin/bash
ubm_name="multi_cmvn"
ivector_dim=25
vaddir="$(pwd)/mfcc"
train_set="wsj_train_si84_multi"
posterior_scale=0.1
. run_extract_ivec_with_vad_cmvn.sh
#compute_vad
awk '{print $1, $1;}' data/$train_set/utt2spk > data/$train_set/utt2utt
./compute_utt_cmvn_stats.sh data/$train_set exp/make_mfcc/$train_set mfcc/
split_by_gender "$train_set" #prepare_data
#train_ubm
#echo "train by gender"
#train_ubm_by_gender
#train_ivector_extractor
extract_ivectors "$train_set" "$train_set" "p$posterior_scale" 40
