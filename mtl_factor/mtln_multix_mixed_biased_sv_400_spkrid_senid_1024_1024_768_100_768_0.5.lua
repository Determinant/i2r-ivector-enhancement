require 'kaldi_io'
gconf = {lrate = 0.8, wcost = 1e-6, momentum = 0.9,
        cumat_type = nerv.CuMatrixFloat,
        mmat_type = nerv.MMatrixFloat,
        tr_scp = "ark:copy-feats scp:data/utt_ivector_rsr_multix_mixed_biased_400_bulk/feats_head.scp ark:- |",
--        cv_scp = "ark:copy-feats scp:data/utt_ivector_rsr_multix_mixed_biased_400_bulk/feats_tail.scp ark:- |",
        cv_scp = "ark:copy-feats scp:rsr_test_ivectors/rsr_utt_ivector_test_multi_mixed_spkrid_senid_0.5_norm.scp ark:- |",
--        cv_scp = "ark:copy-feats scp:rsr_test_ivectors/rsr_utt_ivector_enroll_multi_mixed_spkrid_senid_0.5_norm.scp ark:- |",

        initialized_param = {"testn100_mixed_biased_sv_400_spkrid_senid_0.5/mtl_rbmn_multix_mixed_sv_400_1024_1024_768_100_768_20160119183400_iter_6_lr0.172800_tr0.002_cv0.010.nerv",
--        initialized_param = {"mtl_rbmn_multix_mixed_sv_400_1024_1024_768_100_768.nerv",
--                            "rsr_206_768.nerv", "rsr_30_768.nerv",
							"feature_transformn_multix_mixed_sv_400.nerv"},
        debug = false}

function make_layer_repo(param_repo)
    local layer_repo = nerv.LayerRepo(
    {
        -- global transf
        ["nerv.BiasLayer"] =
        {
            blayer1 = {{bias = "bias0"}, {dim_in = {400}, dim_out = {400}}}
        },
        ["nerv.WindowLayer"] =
        {
            wlayer1 = {{window = "window0"}, {dim_in = {400}, dim_out = {400}}}
        },
        -- biased linearity
        ["nerv.AffineLayer"] =
        {
            affine0 = {{ltp = "affine0_ltp", bp = "affine0_bp"},
            {dim_in = {400}, dim_out = {1024}}},
            affine1 = {{ltp = "affine1_ltp", bp = "affine1_bp"},
            {dim_in = {1024}, dim_out = {1024}}},
            affine2 = {{ltp = "affine2_ltp", bp = "affine2_bp"},
            {dim_in = {1024}, dim_out = {768}}},
            affine3 = {{ltp = "affine3_ltp", bp = "affine3_bp"},
            {dim_in = {768}, dim_out = {100}}},
            affine4 = {{ltp = "affine4_ltp", bp = "affine4_bp"},
            {dim_in = {100}, dim_out = {768}}},

            affine30 = {{ltp = "affine30_ltp", bp = "affine30_bp"},
            {dim_in = {768}, dim_out = {206}}},
            affine20 = {{ltp = "affine20_ltp", bp = "affine20_bp"},
            {dim_in = {768}, dim_out = {30}}}
        },
        ["nerv.SigmoidLayer"] =
        {
            sigmoid0 = {{}, {dim_in = {1024}, dim_out = {1024}}},
            sigmoid1 = {{}, {dim_in = {1024}, dim_out = {1024}}},
            sigmoid2 = {{}, {dim_in = {768}, dim_out = {768}}},
            sigmoid3 = {{}, {dim_in = {100}, dim_out = {100}}},
            sigmoid4 = {{}, {dim_in = {768}, dim_out = {768}}}
        },
        ["nerv.SoftmaxCELayer"] =
        {
            ce_spkr_id  = {{}, {dim_in = {206, 1}, dim_out = {1}, compressed = true}},
            ce_sen_id = {{}, {dim_in = {30, 1}, dim_out = {1}, compressed = true}}
        },
        ["nerv.CombinerLayer"] =
        {
            splitter = {{}, {dim_in = {768}, dim_out = {768, 768}, lambda = {1}}},
            final_crit = {{}, {dim_in = {1, 1}, dim_out = {1}, lambda = {0.5, 0.5}}}
        }
    }, param_repo, gconf)

    layer_repo:add_layers(
    {
        ["nerv.DAGLayer"] =
        {
            global_transf = {{}, {
                dim_in = {400}, dim_out = {400},
                sub_layers = layer_repo,
                connections = {
                    ["<input>[1]"] = "blayer1[1]",
                    ["blayer1[1]"] = "wlayer1[1]",
                    ["wlayer1[1]"] = "<output>[1]"
                }
            }},
            main = {{}, {
                dim_in = {400}, dim_out = {768},
                sub_layers = layer_repo,
                connections = {
                    ["<input>[1]"] = "affine0[1]",
                    ["affine0[1]"] = "sigmoid0[1]",
                    ["sigmoid0[1]"] = "affine1[1]",
                    ["affine1[1]"] = "sigmoid1[1]",
                    ["sigmoid1[1]"] = "affine2[1]",
--                    ["affine2[1]"] = "affine3[1]",
                    ["affine2[1]"] = "sigmoid2[1]",
                    ["sigmoid2[1]"] = "affine3[1]",
                    ["affine3[1]"] = "sigmoid3[1]",
                    ["sigmoid3[1]"] = "affine4[1]",
                    ["affine4[1]"] = "sigmoid4[1]",
                    ["sigmoid4[1]"] = "<output>[1]",
                }
            }}
        }
    }, param_repo, gconf)

    layer_repo:add_layers(
    {
        ["nerv.DAGLayer"] =
        {
            ce_output = {{}, {
                dim_in = {400, 1, 1}, dim_out = {1},
                sub_layers = layer_repo,
                connections = {
                    ["<input>[1]"] = "main[1]",
                    ["main[1]"] = "splitter[1]",

                    ["splitter[1]"] = "affine30[1]",
                    ["affine30[1]"] = "ce_spkr_id[1]",
                    ["<input>[2]"] = "ce_spkr_id[2]",

                    ["splitter[2]"] = "affine20[1]",
                    ["affine20[1]"] = "ce_sen_id[1]",
                    ["<input>[3]"] = "ce_sen_id[2]",

                    ["ce_spkr_id[1]"] = "final_crit[1]",
                    ["ce_sen_id[1]"] = "final_crit[2]",

                    ["final_crit[1]"] = "<output>[1]"
                }
            }},
            ex_output = {{}, {
                dim_in = {400}, dim_out = {768},
                sub_layers = layer_repo,
                connections = {
                    ["<input>[1]"] = "main[1]",
                    ["main[1]"] = "<output>[1]",
                }
            }}
        }
    }, param_repo, gconf)

    return layer_repo
end

function get_network(layer_repo)
    return layer_repo:get_layer("ce_output")
end

function get_global_transf(layer_repo)
    return layer_repo:get_layer("global_transf")
end

function make_readers(scp_file, layer_repo)
    return {
                {reader = nerv.KaldiReader(gconf,
                    {
                        id = "ivector_uttr_rsr_multix",
                        feature_rspecifier = scp_file,
                        mlfs = {},
                        lookup = {
                            rsr_spkr_id = {
                                targets_rspecifier = "ark:copy-feats ark:rsr_sid.ark ark:- |",
                                map_rspecifier = "ark:data/utt_ivector_rsr_multix_mixed_biased_400_bulk/utt2spk"
                            },
                            rsr_sen_id = {
                                targets_rspecifier = "ark:copy-feats ark:rsr_senid.ark ark:- |",
                                map_rspecifier = "ark:data/utt_ivector_rsr_multix_mixed_biased_400_bulk/utt2sen"
                            }
                        },
                        -- global_transf = layer_repo:get_layer("global_transf")
                    }),
                data = {ivector_uttr_rsr_multix = 400, rsr_spkr_id = 1, rsr_sen_id = 1}}
            }
end

function make_ex_readers(scp_file, layer_repo)
    return {
                {reader = nerv.KaldiReader(gconf,
                    {
                        id = "ivector_uttr_rsr_multix",
                        feature_rspecifier = scp_file,
                    }),
                data = {ivector_uttr_rsr_multix = 400}}
            }
end

function make_buffer(readers)
    return nerv.SGDBuffer(gconf,
        {
            buffer_size = gconf.buffer_size,
            randomize = gconf.randomize,
            readers = readers,
            use_gpu = true
        })
end

function get_input_order()
    return {{id = "ivector_uttr_rsr_multix", global_transf = true},
            {id = "rsr_spkr_id"},
            {id = "rsr_sen_id"}}
end

function get_accuracy(layer_repo)
    local ce_spkr_id = layer_repo:get_layer("ce_spkr_id")
    local ce_sen_id = layer_repo:get_layer("ce_sen_id")
    local frames = ce_spkr_id.total_frames
    return (0.5 * ce_spkr_id.total_ce + 0.5 * ce_sen_id.total_ce) / frames
end

function print_stat(layer_repo)
    local ce_spkr_id = layer_repo:get_layer("ce_spkr_id")
    local ce_sen_id = layer_repo:get_layer("ce_sen_id")
    local frames = ce_spkr_id.total_frames
    nerv.info("*** training stat begin ***")
    nerv.printf("frames:\t\t\t%d\n", frames)
    nerv.printf("ce_spkr_id err/frm:\t\t%.8f\n", ce_spkr_id.total_ce / frames)
    nerv.printf("ce_spkr_id accu:\t\t%.3f%%\n", ce_spkr_id.total_correct / frames * 100)
    nerv.printf("ce_sen_id err/frm:\t\t%.8f\n", ce_sen_id.total_ce / frames)
    nerv.printf("ce_sen_id accu:\t\t%.3f%%\n", ce_sen_id.total_correct / frames * 100)
    nerv.info("*** training stat end ***")
end
