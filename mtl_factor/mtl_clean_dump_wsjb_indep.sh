#!/bin/bash
set -x
testsets="test_eval92_clean_indep test_eval93_clean_indep test_dev93_clean_indep"
testsets5k="test_eval92_5k_mfcc_uttn_indep test_eval93_5k_mfcc_uttn_indep test_dev93_5k_mfcc_uttn_indep"
trainsets="train_si284_clean_indep" # dev_0330"
[[ -z "$nn_file" ]] && { echo "nn_file expected"; exit 1; }
[[ -z "$nn_param" ]] && { echo "nn_param expected"; exit 1; }
[[ -z "$transf_file" ]] && { echo "transf_file expected"; exit 1; }
[[ -z "$ivector_dim" ]] && ivector_dim=100

function extract_wsj_train {
    for i in $trainsets; do
        recover_file="wsj_utt_ivector_${i}${name}${nn_suffix}_recover"
        file_prefix="wsj_train_ivectors/wsj_utt_ivector_${i}${name}${nn_suffix}"
        cat exp/ivectors_wsj_${i}${name}_female_${ivector_dim}/ivector.scp | copy-vector scp:- ark,t:- | ivector-normalize-length ark:- "ark,scp,t:${file_prefix}_norm.ark,${file_prefix}_norm.scp"
        ivector-mean ark:data/wsj_${i}/spk2utt scp:${file_prefix}_norm.scp ark:- | ivector-normalize-length ark:- "ark,scp,t:${file_prefix}_spkr_norm.ark,${file_prefix}_spkr_norm.scp"
        ../merge-vector-to-matrix --fpu=512  --recover_map="ark:$recover_file" --utt2spk="ark:data/wsj_${i}/utt2spk" --utt2noise="ark:data/wsj_${i}/utt2spk" "scp:${file_prefix}_norm.scp" ark,scp,t:"${file_prefix}_norm_bulk.ark,${file_prefix}_norm_bulk.scp"
        [[ -z "$dump_clean_spkr" ]] || { nerv mtl_extractor.lua "$nn_file" "$nn_param" "ark:copy-feats scp:${file_prefix}_norm_bulk.scp ark:- |" "$recover_file" "$transf_file" 0
        ivector-mean < mtl_dumped ark:data/wsj_${i}/spk2utt ark:- "ark,scp,t:${file_prefix}_dumped_clean_spkr.ark,${file_prefix}_dumped_clean_spkr.scp"; }
        [[ -z "$dump_noisen" ]] || { nerv mtl_extractor.lua "$nn_file" "$nn_param" "ark:copy-feats scp:${file_prefix}_norm_bulk.scp ark:- |" "$recover_file" "$transf_file" 1
        ivector-normalize-length < mtl_dumped ark:- "ark,scp,t:${file_prefix}_dumped_noisen.ark,${file_prefix}_dumped_noisen.scp"; }
        [[ -z "$dump_btnkn" ]] || {
            nerv mtl_extractor.lua "$nn_file" "$nn_param" "ark:copy-feats scp:${file_prefix}_norm_bulk.scp ark:- |" "$recover_file" "$transf_file" 2 
            ivector-normalize-length < mtl_dumped ark:- "ark,scp,t:${file_prefix}_dumped_btnkn.ark,${file_prefix}_dumped_btnkn.scp"
            copy-vector < mtl_dumped ark:- "ark,scp,t:${file_prefix}_dumped_btnk.ark,${file_prefix}_dumped_btnk.scp"
        }
    done
}

function extract_wsj_test {
    for i in $testsets; do
        recover_file="wsj_utt_ivector_test_${i}${name}${nn_suffix}_recover"
        file_prefix="wsj_test_ivectors/wsj_utt_ivector_test_${i}${name}${nn_suffix}"
        cat exp/ivectors_wsj_${i}${name}_female_${ivector_dim}/ivector.scp | copy-vector scp:- ark,t:- | ivector-normalize-length ark:- "ark,scp,t:${file_prefix}_norm.ark,${file_prefix}_norm.scp"
        ivector-mean ark:data/wsj_${i}/spk2utt scp:${file_prefix}_norm.scp ark:- | ivector-normalize-length ark:- "ark,scp,t:${file_prefix}_spkr_norm.ark,${file_prefix}_spkr_norm.scp"
        ../merge-vector-to-matrix --fpu=512  --recover_map="ark:$recover_file" --utt2spk="ark:data/wsj_${i}/utt2spk" --utt2noise="ark:data/wsj_${i}/utt2spk" "scp:${file_prefix}_norm.scp" ark,scp,t:"${file_prefix}_norm_bulk.ark,${file_prefix}_norm_bulk.scp"
        [[ -z "$dump_clean_spkr" ]] || { nerv mtl_extractor.lua "$nn_file" "$nn_param" "ark:copy-feats scp:${file_prefix}_norm_bulk.scp ark:- |" "$recover_file" "$transf_file" 0
        ivector-mean < mtl_dumped ark:data/wsj_${i}/spk2utt ark:- "ark,scp,t:${file_prefix}_dumped_clean_spkr.ark,${file_prefix}_dumped_clean_spkr.scp"; }
        [[ -z "$dump_noisen" ]] || { nerv mtl_extractor.lua "$nn_file" "$nn_param" "ark:copy-feats scp:${file_prefix}_norm_bulk.scp ark:- |" "$recover_file" "$transf_file" 1 
        ivector-normalize-length < mtl_dumped ark:- "ark,scp,t:${file_prefix}_dumped_noisen.ark,${file_prefix}_dumped_noisen.scp"; }
        [[ -z "$dump_btnkn" ]] || { nerv mtl_extractor.lua "$nn_file" "$nn_param" "ark:copy-feats scp:${file_prefix}_norm_bulk.scp ark:- |" "$recover_file" "$transf_file" 2 
            ivector-normalize-length < mtl_dumped ark:- "ark,scp,t:${file_prefix}_dumped_btnkn.ark,${file_prefix}_dumped_btnkn.scp"
            copy-vector < mtl_dumped ark:- "ark,scp,t:${file_prefix}_dumped_btnk.ark,${file_prefix}_dumped_btnk.scp"
        }
    done
}

function extract_wsj_test_5k {
    for i in $testsets5k; do
        recover_file="wsj_utt_ivector_test_${i}${name}${nn_suffix}_recover"
        file_prefix="wsj_test_ivectors/wsj_utt_ivector_test_${i}${name}${nn_suffix}"
        cat exp/ivectors_wsj_${i}${name}_female_${ivector_dim}/ivector.scp | copy-vector scp:- ark,t:- | ivector-normalize-length ark:- "ark,scp,t:${file_prefix}_norm.ark,${file_prefix}_norm.scp"
        ivector-mean ark:data/wsj_${i}/spk2utt scp:${file_prefix}_norm.scp ark:- | ivector-normalize-length ark:- "ark,scp,t:${file_prefix}_spkr_norm.ark,${file_prefix}_spkr_norm.scp"
        ../merge-vector-to-matrix --fpu=512  --recover_map="ark:$recover_file" --utt2spk="ark:data/wsj_${i}/utt2spk" --utt2noise="ark:data/wsj_${i}/utt2spk" "scp:${file_prefix}_norm.scp" ark,scp,t:"${file_prefix}_norm_bulk.ark,${file_prefix}_norm_bulk.scp"
        [[ -z "$dump_clean_spkr" ]] || { nerv mtl_extractor.lua "$nn_file" "$nn_param" "ark:copy-feats scp:${file_prefix}_norm_bulk.scp ark:- |" "$recover_file" "$transf_file" 0
        ivector-mean < mtl_dumped ark:data/wsj_${i}/spk2utt ark:- "ark,scp,t:${file_prefix}_dumped_clean_spkr.ark,${file_prefix}_dumped_clean_spkr.scp"; }
        [[ -z "$dump_noisen" ]] || { nerv mtl_extractor.lua "$nn_file" "$nn_param" "ark:copy-feats scp:${file_prefix}_norm_bulk.scp ark:- |" "$recover_file" "$transf_file" 1 
        ivector-normalize-length < mtl_dumped ark:- "ark,scp,t:${file_prefix}_dumped_noisen.ark,${file_prefix}_dumped_noisen.scp"; }
        [[ -z "$dump_btnkn" ]] || { nerv mtl_extractor.lua "$nn_file" "$nn_param" "ark:copy-feats scp:${file_prefix}_norm_bulk.scp ark:- |" "$recover_file" "$transf_file" 2 
            ivector-normalize-length < mtl_dumped ark:- "ark,scp,t:${file_prefix}_dumped_btnkn.ark,${file_prefix}_dumped_btnkn.scp"
            copy-vector < mtl_dumped ark:- "ark,scp,t:${file_prefix}_dumped_btnk.ark,${file_prefix}_dumped_btnk.scp"
        }
    done
}
