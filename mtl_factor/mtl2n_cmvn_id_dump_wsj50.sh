#!/bin/bash
. mtl_dump_wsj.sh
nn_param="test2n60cmvn_id/mtl_rbm2n_cmvn_1024_768_60_768_1024_20150914172613_iter_36_lr0.000226_tr0.193_cv0.420.nerv"
nn_file="mtl2n_cmvn_id_1024_768_60_768_1024.lua"
name="_multi_cmvnp0.1"
nn_suffix="_id_u50"
ivector_dim=50
#dump_btnkn=true
extract_wsj_train
extract_wsj_test
