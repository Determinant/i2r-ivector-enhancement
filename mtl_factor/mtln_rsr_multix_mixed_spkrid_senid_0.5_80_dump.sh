#!/bin/bash
nn_param="testn80_mixed_sv_400_spkrid_senid_0.5/mtl_rbmn_multix_mixed_sv_400_1024_1024_768_80_768_20160106170927_iter_6_lr0.172800_tr0.003_cv0.011.nerv"
nn_file="mtln_multix_mixed_sv_400_spkrid_senid_1024_1024_768_80_768_0.5.lua"
transf_file="feature_transformn_multix_mixed_sv_400.nerv"
name="_multi"
nn_suffix="_mixed_spkrid_senid_0.5"
datadir="rsr_test_mixed"
dump_btnkn=true
. mtl_dump_rsr.sh
layer_id=affine3
dump_id=id_0.5_80
dump_type=2
extract_rsr_test prepare
extract_rsr_test dump_custom
