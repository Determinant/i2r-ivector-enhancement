function trim2(s)
    return s:match "^%s*(.-)%s*$"
end
function string:split(sSeparator, nMax, bRegexp)
    assert(sSeparator ~= '')
    assert(nMax == nil or nMax >= 1)
    
    local aRecord = {}
    
    if self:len() > 0 then
        local bPlain = not bRegexp
        nMax = nMax or -1
        
        local nField, nStart = 1, 1
        local nFirst,nLast = self:find(sSeparator, nStart, bPlain)
        while nFirst and nMax ~= 0 do
            aRecord[nField] = self:sub(nStart, nFirst-1)
            nField = nField+1
            nStart = nLast+1
            nFirst,nLast = self:find(sSeparator, nStart, bPlain)
            nMax = nMax-1
        end
        aRecord[nField] = self:sub(nStart)
    end
    
    return aRecord
end

dofile(arg[1])
--gconf.batch_size = 1
gconf.batch_size = 256
gconf.buffer_size = 81920

function make_buffer(readers)
    return nerv.SGDBuffer(gconf,
        {
            buffer_size = gconf.buffer_size,
            randomize = gconf.randomize,
            readers = readers,
            use_gpu = true,
            consume = true
        })
end
--scp_file = "ark:copy-feats scp:data/aurora4_utt_ivector_multin_100_bulk/feats.scp ark:- |"
final_param = arg[2] or nerv.error("final_param expected")
scp_file = arg[3] or nerv.error("scp_file expected")
recover_file = arg[4] or nerv.error("recover_file expected")
transf_param = arg[5] or nerv.error("transf_param expected")
dump_type = arg[6] or nerv.error("dump_type expected")
fea_id = arg[7] or "ivector_uttr_noisy"
btnk_id = arg[8] or "affine2"
output_file = arg[9] or "mtl_dumped"

ofp = io.open(output_file, "w")

param_repo = nerv.ParamRepo()
param_repo:import({final_param, transf_param}, nil, gconf)
layer_repo = make_layer_repo(param_repo)
network = layer_repo:get_layer("ex_output")
network_main = layer_repo:get_layer("main")
global_transf = get_global_transf(layer_repo)
input_order = {
    {id = fea_id, global_transf = true}
}
readers = make_ex_readers(scp_file, layer_repo)
buffer = make_buffer(readers)
reader = readers[1].reader
network:init(gconf.batch_size)
gconf.cnt = 0
recover_order = {}
for line in io.lines(recover_file) do
    local splitted = string.split(trim2(line), " ")
    local utter_key = splitted[1]
    local utter_list = {}
    for i = 5, #splitted do
        table.insert(recover_order, splitted[i])
    end
end

cnt = 1
previous_batch_size = gconf.batch_size
for data in buffer.get_data, buffer do
    local utter = data[fea_id]
    local input = {}
    for i, e in ipairs(input_order) do
        local id = e.id
        if data[id] == nil then
            nerv.error("input data %s not found", id)
        end
        local transformed
        if e.global_transf then
            transformed = nerv.speech_utils.global_transf(data[id],
                                global_transf,
                                gconf.frm_ext or 0,
                                gconf.frm_trim or 0,
                                gconf)
        else
            transformed = data[id]
        end
        table.insert(input, transformed)
    end
    local batch_size = input[1]:nrow()
    if batch_size ~= previous_batch_size then
        network:init(batch_size)
        previous_batch_size = batch_size
    end
    local output = {}
    for i = 1, #network.dim_out do
        table.insert(output, nerv.CuMatrixFloat(batch_size, network.dim_out[i]))
    end
    network:propagate(input, output)
    local pred
    if dump_type == "0" then
        pred = output[1]
    elseif dump_type == "1" then
        pred = output[2]
    else
        pred = network_main:get_intermediate(btnk_id, nerv.DAGLayer.PORT_TYPES.OUTPUT)[1]
    end
    for i = 0, pred:nrow() - 1 do
        local frame = pred[i]
--        local d = {reader.feat_repo:key() .. " [ "}
        local d = {recover_order[cnt] .. " [ "}
        for j = 0, frame:ncol() - 1 do
            table.insert(d, string.format("%.8f ", frame[j]))
        end
        table.insert(d, "]")
        ofp:write(table.concat(d) .. "\n")
        cnt = cnt + 1
    end
end
print(#recover_order, cnt - 1)
