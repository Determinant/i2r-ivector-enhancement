#!/bin/bash
utt2spk=$1
utt2noise=$2
[[ -z "$utt2spk" ]] && exit 1
[[ -z "$utt2noise" ]] && exit 1
awk 'FNR==NR{ key[$1] = $2; } FNR!=NR{ printf "%s %s_%s\n", $1, key[$1], $2; }' "$utt2spk" "$utt2noise"
