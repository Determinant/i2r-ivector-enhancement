#!/bin/bash
. mtl_dump_wsj.sh
nn_param="test2n60cmvnsig_jid/mtl_rbm2n_cmvn_sig_1024_768_60_768_1024_20150921133329_iter_37_lr0.000081_tr0.226_cv1.474.nerv"
nn_file="mtl2n_cmvn_sig_jid_1024_768_60_768_1024.lua"
name="_multi_cmvnp0.1"
nn_suffix="_sjid"
dump_btnkn=true
extract_wsj_train
extract_wsj_test
