#!/bin/bash
ubm_name="multi_cmvn"
ivector_dim=100
vaddir="$(pwd)/mfcc"
train_set="train_si284_multi"
posterior_scale=0.1
dont_fix=true
. run_extract_ivec_with_vad_cmvn.sh
#compute_vad
prepare_data
train_ubm
echo "train by gender"
train_ubm_by_gender
train_ivector_extractor "p$posterior_scale"

train_set="train_si284_multi2"
split_by_gender "$train_set"
extract_ivectors "$train_set" "$train_set" "p$posterior_scale" 48
