#!/bin/bash
nn_param="test2n60cmvn_0.5/mtl_rbm2n_cmvn_1024_768_60_768_1024_20150925205638_iter_30_lr0.004837_tr0.121_cv0.181.nerv"
nn_file="mtl2n_cmvn_1024_768_60_768_1024_0.5.lua"
transf_file="feature_transform2n_cmvn.nerv"
name="_multi_cmvnp0.1"
nn_suffix="_mse_0.5"
dump_btnkn=true
. mtl_dump_wsj.sh
extract_wsj_train
extract_wsj_test_5k
