#!/bin/bash
ubm_name="noise_cmvn"
ivector_dim=10
gmm_num=512
train_set=noise
posterior_scale=0.1
. run_extract_noise_ivec_cmvn.sh
train_ubm
train_ivector_extractor "p$posterior_scale"
extract_ivectors noise noise "p$posterior_scale" 8
