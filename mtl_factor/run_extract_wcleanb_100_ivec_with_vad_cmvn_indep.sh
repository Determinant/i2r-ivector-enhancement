#!/bin/bash
ubm_name="clean_cmvn_indep"
ivector_dim=100
vaddir="$(pwd)/mfcc"
posterior_scale=0.1
train_set="train_si284_clean_indep"
indep=true
. run_extract_ivec_with_vad_cmvn.sh
#compute_vad
#prepare_data
#train_ubm
echo "train by gender"
#train_ubm_by_gender
train_ivector_extractor

train_set="wsj_train_si284_clean_indep"
awk '{print $1, $1;}' data/$train_set/utt2spk > data/$train_set/utt2utt
./compute_utt_cmvn_stats.sh data/$train_set exp/make_mfcc/$train_set mfcc/
split_by_gender "$train_set" #prepare_data
extract_ivectors "$train_set" "$train_set" "" 40
