#!/bin/bash
ubm_name="multi_cmvn"
ivector_dim=100
vaddir="$(pwd)/mfcc"
posterior_scale=0.1
. run_extract_ivec_with_vad_cmvn.sh
for x in clean airport babble car restaurant street train ; do
for wv in wv1 wv2; do
train_set="aurora4_${x}_${wv}"
awk '{print $1, $1;}' data/$train_set/utt2spk > data/$train_set/utt2utt
./compute_utt_cmvn_stats.sh data/$train_set exp/make_mfcc/$train_set mfcc/
##sed -i "s=mfcc/vad_aurora4_${x}_${wv}=mfcc/vad_aurora4_clean_${wv}=g" data/$train_set/vad.scp
#sed -i "s=mfcc/vad_aurora4_clean_wv1=mfcc/vad_aurora4_clean_${wv}=g" data/$train_set/vad.scp
#compute_vad
split_by_gender "$train_set" #prepare_data
#train_ubm
#echo "train by gender"
#train_ubm_by_gender
#train_ivector_extractor
extract_ivectors "$train_set" "$train_set" "p$posterior_scale" 2
done
done
