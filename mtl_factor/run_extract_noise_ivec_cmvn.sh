#!/bin/bash
# Copyright 2013   Daniel Povey
#           2014   David Snyder
#           2015   Ted Yin
# Apache 2.0.
#
# See README.txt for more info on data required.
# Results (EERs) are inline in comments below.

# This example script is still a bit of a mess, and needs to be
# cleaned up, but it shows you all the basic ingredients.

. cmd.sh
. path.sh

set -e

[[ -z "$ubm_name" ]] && exit 1
[[ -z "$ivector_dim" ]] && ivector_dim=100
[[ -z "$gmm_num" ]] && gmm_num=${gmm_num}

function train_ubm {
    # The recipe currently uses delta-window=3 and delta-order=2. However
    # the accuracy is almost as good using delta-window=4 and delta-order=1
    # and could be faster due to lower dimensional features.  Alternative
    # delta options (e.g., --delta-window 4 --delta-order 1) can be provided to
    # sid/train_diag_ubm.sh.  The options will be propagated to the other scripts.
    [[ -z "$train_set" ]] && exit 1
    ./train_diag_ubm_cmvn.sh --nj 8 --cmd "$train_cmd" data/${train_set} ${gmm_num} \
        exp/diag_ubm_${gmm_num}_${ubm_name}

    ./train_full_ubm_cmvn.sh --nj 8 --cmd "$train_cmd" data/${train_set} \
        exp/diag_ubm_${gmm_num}_${ubm_name} exp/full_ubm_${gmm_num}_${ubm_name}
}

function train_ivector_extractor {
    local extractor_suffix="$1"
    [[ -z "$train_set" ]] && exit 1
    ./train_ivector_extractor_cmvn.sh --nj 1 --cmd "$train_cmd -l mem_free=8G,ram_free=8G" \
        ${posterior_scale:+--posterior_scale $posterior_scale} \
        --num-iters 5 --ivector-dim $ivector_dim exp/full_ubm_${gmm_num}_${ubm_name}/final.ubm data/${train_set} \
        exp/extractor_${gmm_num}_${ubm_name}${extractor_suffix}_$ivector_dim
}

# Extract the iVectors for training data

function extract_ivectors {
    local data_set="$1"
    local data_name="$2"
    local extractor_suffix="$3"
    local nj="$4"
    [[ -z "$data_set" ]] && exit 1
    [[ -z "$data_name" ]] && exit 1
    [[ -z "$nj" ]] && nj=20
    ./extract_ivectors_cmvn.sh --cmd "$train_cmd -l mem_free=3G,ram_free=3G" --nj $nj \
        ${posterior_scale:+--posterior_scale $posterior_scale} \
        exp/extractor_${gmm_num}_${ubm_name}${extractor_suffix}_$ivector_dim data/${data_set} exp/ivectors_${data_name}_${ubm_name}${extractor_suffix}_$ivector_dim
}
