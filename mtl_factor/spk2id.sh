#!/bin/bash
spklist="$1"
[[ -z "$spklist" ]] && spklist=data/train_si284_multi2/spk2gender
lines=$(wc -l "$spklist" | awk '{print $1}')
echo "$lines"
#awk 'BEGIN {cnt = 0; } { printf "%s [", $1; for (i=0; i<cnt; i++) printf " 0"; printf " 1"; for (i=cnt+1;i<'"$lines"';i++) printf " 0"; printf " ]\n"; cnt++; }' "$spklist"
awk 'BEGIN {cnt = 0; } { printf "%s [ %d ]\n", $1, cnt++; }' "$spklist"
