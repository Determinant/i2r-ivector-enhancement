#!/bin/bash
set -x
[[ -z "$datadir" ]] && datadir="rsr_test"
datasets="enroll test"
[[ -z "$nn_file" ]] && { echo "nn_file expected"; exit 1; }
[[ -z "$nn_param" ]] && { echo "nn_param expected"; exit 1; }
[[ -z "$transf_file" ]] && { echo "transf_file expected"; exit 1; }
[[ -z "$ivector_dim" ]] && ivector_dim=400

function dump_custom {
    [[ -z "$layer_id" ]] && { echo "layer_id expected"; exit 1; }
    [[ -z "$dump_id" ]] && { echo "dump_id expected"; exit 1; }
    [[ -z "$dump_type" ]] && { echo "dump_type expected"; exit 1; }
    nerv mtl_extractor.lua "$nn_file" "$nn_param" "ark:copy-feats scp:${file_prefix}_norm_bulk.scp ark:- |" "$recover_file" "$transf_file" "$dump_type" ivector_uttr_rsr_multix "$layer_id"
    ivector-normalize-length < mtl_dumped ark:- "ark,scp,t:${file_prefix}_dumped_${dump_id}n.ark,${file_prefix}_dumped_${dump_id}n.scp"
    copy-vector < mtl_dumped ark:- "ark,scp,t:${file_prefix}_dumped_${dump_id}.ark,${file_prefix}_dumped_${dump_id}.scp"
}

function prepare {
    ivector-normalize-length "ark:data/${datadir}/${i}.ark" "ark,scp,t:${file_prefix}_norm.ark,${file_prefix}_norm.scp"
    #ivector-mean ark:data/wsj_${i}/spk2utt scp:${file_prefix}_norm.scp ark:- | ivector-normalize-length ark:- "ark,scp,t:${file_prefix}_spkr_norm.ark,${file_prefix}_spkr_norm.scp"
    ../merge-vector-to-matrix --fpu=512  --recover_map="ark:$recover_file" \
        --utt2spk="ark:data/${datadir}/utt2spk" \
        --utt2noise="ark:data/${datadir}/utt2noise" \
        "scp:${file_prefix}_norm.scp" \
        ark,scp,t:"${file_prefix}_norm_bulk.ark,${file_prefix}_norm_bulk.scp"
}

function extract_rsr_test {
    basedir='rsr_test_ivectors'
    mkdir -p "$basedir"
    for i in $datasets; do
        recover_file="rsr_utt_ivector_${i}${name}${nn_suffix}_recover"
        file_prefix="$basedir/rsr_utt_ivector_${i}${name}${nn_suffix}"
        $1
    done
}
