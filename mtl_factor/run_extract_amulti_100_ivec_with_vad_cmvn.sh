#!/bin/bash
ubm_name="multi_cmvn"
ivector_dim=100
vaddir="$(pwd)/mfcc"
train_set="aurora4_train_si84_multi"
posterior_scale=0.1
. run_extract_ivec_with_vad_cmvn.sh
#compute_vad
split_by_gender "$train_set" #prepare_data
#train_ubm
#echo "train by gender"
#train_ubm_by_gender
#train_ivector_extractor
extract_ivectors "$train_set" "$train_set" "p$posterior_scale" 40
