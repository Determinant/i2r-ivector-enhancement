#!/bin/bash
ivector_scp=train_ivector/spk_ivector_multi.scp
ivector_map=train_utt2spk/utt2spk_multi
nn_name=multi
tr_env=multi
dec_ivector_scp=test_mc_spk_ivector.scp
dec_ivector_map=test_mc_utt2spk
. run_with_ivec.sh

rbm_pretrain
nn_train
decode
