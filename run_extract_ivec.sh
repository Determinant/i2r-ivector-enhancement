#!/bin/bash
# Copyright 2013   Daniel Povey
#           2014   David Snyder
#           2015   Ted Yin
# Apache 2.0.
#
# See README.txt for more info on data required.
# Results (EERs) are inline in comments below.

# This example script is still a bit of a mess, and needs to be
# cleaned up, but it shows you all the basic ingredients.

. cmd.sh
. path.sh

set -e

[[ -z "$ubm_name" ]] && exit 1
[[ -z "$ivector_dim" ]] && ivector_dim=100

# Split test data by gender
function split_by_gender {
    local data="$1"
    [[ -z "$data" ]] && exit 1
    grep -w m data/$data/spk2gender | awk '{print $1}' > foo;
    utils/subset_data_dir.sh --spk-list foo data/$data data/${data}_male
    grep -w f data/$data/spk2gender | awk '{print $1}' > foo;
    utils/subset_data_dir.sh --spk-list foo data/$data data/${data}_female
    rm foo
    utils/fix_data_dir.sh data/${data}_male
    utils/fix_data_dir.sh data/${data}_female
}

# Data preparation
function prepare_data {
    [[ -z "$train_set" ]] && exit 1
    # Get male and female subsets of training data.
    split_by_gender "$train_set"
    # Get smaller subsets of training data for faster training.
    utils/subset_data_dir.sh data/$train_set 4000 data/${train_set}_4k
    #utils/subset_data_dir.sh data/$train_set 8000 data/${train_set}_8k
    #utils/subset_data_dir.sh data/${train_set}_male 8000 data/${train_set}_male_8k
    #utils/subset_data_dir.sh data/${train_set}_female 8000 data/${train_set}_female_8k
}

function train_ubm {
    # The recipe currently uses delta-window=3 and delta-order=2. However
    # the accuracy is almost as good using delta-window=4 and delta-order=1
    # and could be faster due to lower dimensional features.  Alternative
    # delta options (e.g., --delta-window 4 --delta-order 1) can be provided to
    # sid/train_diag_ubm.sh.  The options will be propagated to the other scripts.
    [[ -z "$train_set" ]] && exit 1
    ./train_diag_ubm.sh --nj 30 --cmd "$train_cmd" data/${train_set}_4k 2048 \
        exp/diag_ubm_2048_${ubm_name}

    ./train_full_ubm.sh --nj 30 --cmd "$train_cmd" data/${train_set} \
        exp/diag_ubm_2048_${ubm_name} exp/full_ubm_2048_${ubm_name}
}

function train_ubm_by_gender {
    # Get male and female versions of the UBM in one pass; make sure not to remove
    # any Gaussians due to low counts (so they stay matched).  This will be 
    # more convenient for gender-id.
    [[ -z "$train_set" ]] && exit 1
    ./train_full_ubm.sh --nj 30 --remove-low-count-gaussians false \
        --num-iters 1 --cmd "$train_cmd" \
        data/${train_set}_male exp/full_ubm_2048_${ubm_name} exp/full_ubm_2048_${ubm_name}_male &

    ./train_full_ubm.sh --nj 30 --remove-low-count-gaussians false \
        --num-iters 1 --cmd "$train_cmd" \
        data/${train_set}_female exp/full_ubm_2048_${ubm_name} exp/full_ubm_2048_${ubm_name}_female &
    wait
}

function train_ivector_extractor {
    # Train the iVector extractor for male speakers.
    [[ -z "$train_set" ]] && exit 1
    ./train_ivector_extractor.sh --cmd "$train_cmd -l mem_free=8G,ram_free=8G" \
        --num-iters 5 --ivector-dim $ivector_dim exp/full_ubm_2048_${ubm_name}_male/final.ubm data/${train_set}_male \
        exp/extractor_2048_${ubm_name}_male_$ivector_dim

    # The same for female speakers.
    ./train_ivector_extractor.sh --cmd "$train_cmd -l mem_free=8G,ram_free=8G" \
        --num-iters 5 --ivector-dim $ivector_dim exp/full_ubm_2048_${ubm_name}_female/final.ubm data/${train_set}_female \
        exp/extractor_2048_${ubm_name}_female_$ivector_dim
}

function test_gender_id {
    [[ -z "$train_set" ]] && exit 1
    # The script below demonstrates the gender-id script.  We don't really use
    # it for anything here, because the SRE 2008 data is already split up by
    # gender and gender identification is not required for the eval.
    # It prints out the error rate based on the info in the spk2gender file;
    # see exp/gender_id_fisher/error_rate where it is also printed.
    ./gender_id.sh --cmd "$train_cmd" --nj 20 exp/full_ubm_2048_${ubm_name}{,_male,_female} \
        data/$train_set exp/gender_id_${train_set}
}

# Extract the iVectors for training data

function extract_ivectors {
    local data_set="$1"
    local data_name="$2"
    local nj="$3"
    [[ -z "$data_set" ]] && exit 1
    [[ -z "$data_name" ]] && exit 1
    [[ -z "$nj" ]] && nj=20
    ./extract_ivectors.sh --cmd "$train_cmd -l mem_free=3G,ram_free=3G" --nj $nj \
        exp/extractor_2048_${ubm_name}_male_$ivector_dim data/${data_set}_male exp/ivectors_${data_name}_male_$ivector_dim

    ./extract_ivectors.sh --cmd "$train_cmd -l mem_free=3G,ram_free=3G" --nj $nj \
        exp/extractor_2048_${ubm_name}_female_$ivector_dim data/${data_set}_female exp/ivectors_${data_name}_female_$ivector_dim
}
