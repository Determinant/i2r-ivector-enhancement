#!/bin/bash

. ./cmd.sh

# Train and test MMI (and boosted MMI) on tri2b_multi system.
true && {
steps/make_denlats.sh --sub-split 20 --nj 10 --cmd "$train_cmd" \
  data/train_si84_multi data/lang exp/tri2b_multi exp/tri2b_multi_denlats_si84 || exit 1;
}

true && {
# train the basic MMI system.
steps/train_mmi.sh --cmd "$train_cmd" \
  data/train_si84_multi data/lang exp/tri2b_multi_ali_si84 \
  exp/tri2b_multi_denlats_si84 exp/tri2b_multi_mmi  || exit 1;
for iter in 3 4; do
#  steps/decode_si.sh --nj 10 --cmd "$decode_cmd" --iter $iter \
#    exp/tri2b_multi/graph_tgpr data/test_dev93 exp/tri2b_multi_mmi/decode_tgpr_dev93_it$iter &
  steps/decode_si.sh --nj 8 --cmd "$decode_cmd" --iter $iter \
     exp/tri2b_multi/graph_tgpr_5k data/test_eval92 exp/tri2b_multi_mmi/decode_tgpr_5k_eval92_it$iter &
done
}
# MMI with 0.1 boosting factor.
steps/train_mmi.sh --cmd "$train_cmd" --boost 0.1 \
  data/train_si84_multi data/lang exp/tri2b_multi_ali_si84 exp/tri2b_multi_denlats_si84 \
  exp/tri2b_multi_mmi_b0.1  || exit 1;
testsets="airport_wv1 airport_wv2 babble_wv1 babble_wv2 car_wv1 car_wv2 clean_wv1 clean_wv2 restaurant_wv1 restaurant_wv2 street_wv1 street_wv2 train_wv1 train_wv2"
for iter in 3 ; do
 for test in $testsets; do
  steps/decode_si.sh --nj 8 --num-threads 4 --cmd "$decode_cmd" --iter $iter \
     exp/tri2b_multi/graph_tgpr_5k data/$test exp/tri2b_multi_mmi_b0.1/decode_tgpr_5k_${test}_it$iter 
 done
done

false && {
# Train a UBM with 400 components, for fMMI.
steps/train_diag_ubm.sh --silence-weight 0.5 --nj 10 --cmd "$train_cmd" \
  400 data/train_si84_multi data/lang exp/tri2b_multi_ali_si84 exp/dubm2b

 steps/train_mmi_fmmi.sh --boost 0.1 --cmd "$train_cmd" \
   data/train_si84_multi data/lang exp/tri2b_multi_ali_si84 exp/dubm2b exp/tri2b_multi_denlats_si84 \
   exp/tri2b_multi_fmmi_b0.1

 for iter in `seq 3 8`; do 
   steps/decode_fmmi.sh --nj 10 --cmd "$decode_cmd" --iter $iter \
     exp/tri2b_multi/graph_tgpr data/test_dev93 exp/tri2b_multi_fmmi_b0.1/decode_tgpr_dev93_it$iter &
 done

 steps/train_mmi_fmmi.sh --learning-rate 0.005 --boost 0.1 --cmd "$train_cmd" \
   data/train_si84_multi data/lang exp/tri2b_multi_ali_si84 exp/dubm2b exp/tri2b_multi_denlats_si84 \
   exp/tri2b_multi_fmmi_b0.1_lr0.005 || exit 1;
 for iter in `seq 3 8`; do 
   steps/decode_fmmi.sh --nj 10 --cmd "$decode_cmd" --iter $iter \
     exp/tri2b_multi/graph_tgpr data/test_dev93 exp/tri2b_multi_fmmi_b0.1_lr0.005/decode_tgpr_dev93_it$iter &
 done

 steps/train_mmi_fmmi_indirect.sh --boost 0.1 --cmd "$train_cmd" \
   data/train_si84_multi data/lang exp/tri2b_multi_ali_si84 exp/dubm2b exp/tri2b_multi_denlats_si84 \
   exp/tri2b_multi_fmmi_indirect_b0.1
 for iter in `seq 3 8`; do 
   steps/decode_fmmi.sh --nj 10 --cmd "$decode_cmd" --iter $iter \
      exp/tri2b_multi/graph_tgpr data/test_dev93 exp/tri2b_multi_fmmi_indirect_b0.1/decode_tgpr_dev93_it$iter &
 done
}
