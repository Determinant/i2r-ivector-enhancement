KALDI_BASEDIR := /home/stuymf/kaldi-trunk
include $(KALDI_BASEDIR)/src/kaldi.mk
KI := -I $(KALDI_BASEDIR)/src/ -I $(KALDI_BASEDIR)/tools/ATLAS/include -I $(KALDI_BASEDIR)/tools/openfst/include/
KL := $(KALDI_BASEDIR)/src/feat/kaldi-feat.a $(KALDI_BASEDIR)/src/matrix/kaldi-matrix.a $(KALDI_BASEDIR)/src/base/kaldi-base.a $(KALDI_BASEDIR)/src/util/kaldi-util.a $(KALDI_BASEDIR)/src/hmm/kaldi-hmm.a  $(KALDI_BASEDIR)/src/tree/kaldi-tree.a /usr/lib/libatlas.so.3 /usr/lib/libf77blas.so.3 /usr/lib/libcblas.so.3 /usr/lib/liblapack_atlas.so.3

.PHONY: all
.DEFAULT_GOAL := all
all: append-ivectors copy-vector-to-matrix merge-vector-to-matrix merge-vector-to-matrix2

append-ivectors: append-ivectors.cc
	g++ -o $@ $< $(KL) $(KI) -DHAVE_ATLAS -DHAVE_POSIX_MEMALIGN
copy-vector-to-matrix: copy-vector-to-matrix.cc
	g++ -o $@ $< $(KL) $(KI) -DHAVE_ATLAS -DHAVE_POSIX_MEMALIGN
merge-vector-to-matrix: merge-vector-to-matrix.cc
	g++ -o $@ $< $(KL) $(KI) -DHAVE_ATLAS -DHAVE_POSIX_MEMALIGN
merge-vector-to-matrix2: merge-vector-to-matrix2.cc
	g++ -o $@ $< $(KL) $(KI) -DHAVE_ATLAS -DHAVE_POSIX_MEMALIGN
