#!/bin/bash

. ./cmd.sh ## You'll want to change cmd.sh to something that will work on your system.
           ## This relates to the queue.

. ./path.sh
# This is a shell script, but it's recommended that you run the commands one by
# one by copying and pasting into the shell.

splice=5

[[ -z "$nn_name" ]] && exit 1
[[ -z "$tr_env" ]] && exit 1
[[ -z "$ivector_scp" ]] && exit 1
[[ -z "$ivector_map" ]] && exit 1
[[ -z "$ivector_dim" ]] && ivector_dim=100
[[ -z "$ivector_scale" ]] && ivector_scale=$(python -c "import math; print math.sqrt(2) / math.sqrt($ivector_dim)")
[[ -z "$lm_data" ]] && lm_data=tgpr_5k

function rbm_pretrain {
    echo "ivector scaling factor: $ivector_scale"
    #Now begin train DNN systems on data
    #RBM pretrain
    dir=exp/tri3a_dnn2048_${nn_name}_cmn_delta_pretrain_with_ivec
        $cuda_cmd $dir/_pretrain_dbn.log \
        ./pretrain_dbn_with_ivec.sh  --apply-cmvn "true" --norm-vars "false" --delta-order 2 \
        --hid-dim 2048 --nn-depth 7 --rbm-iter 5 \
        --ivector-dim $ivector_dim --ivector-scp $ivector_scp --ivector-map $ivector_map --ivector-scale $ivector_scale \
        data-fbank/train_si84_${tr_env} $dir
}

function nn_train {
    dir=exp/tri3a_dnn2048_${nn_name}_cmn_delta_with_ivec
    ali=exp/tri2b_${tr_env}_mmi_b0.1_ali_si84
    ali_dev=exp/tri2b_${tr_env}_mmi_b0.1_ali_dev_0330
    feature_transform=exp/tri3a_dnn2048_${nn_name}_cmn_delta_pretrain_with_ivec/final.feature_transform
    dbn=exp/tri3a_dnn2048_${nn_name}_cmn_delta_pretrain_with_ivec/7.dbn
    $cuda_cmd $dir/_train_nnet.log \
      ./train_with_ivec.sh --apply-cmvn "true" --norm-vars "false" --delta-order 2 --randomizer-size 200000 \
        --feature-transform $feature_transform --dbn $dbn --hid-layers 0 --learn-rate 0.008 \
        --ivector-dim $ivector_dim --ivector-scp $ivector_scp --ivector-map $ivector_map --ivector-scale $ivector_scale \
          data-fbank/train_si84_${tr_env} data-fbank/dev_0330 data/lang $ali $ali_dev $dir || exit 1;
}

function multi_env_decode {
    [[ -z "$dec_env" ]] && exit 1
    utils/mkgraph.sh data/lang_test_${lm_data} exp/tri3a_dnn2048_${nn_name}_cmn_delta_with_ivec exp/tri3a_dnn2048_${nn_name}_cmn_delta_with_ivec/graph_${lm_data} || exit 1;
    dir=exp/tri3a_dnn2048_${nn_name}_cmn_delta_with_ivec
    testsets="airport_wv1 airport_wv2 babble_wv1 babble_wv2 car_wv1 car_wv2 clean_wv1 clean_wv2 restaurant_wv1 restaurant_wv2 street_wv1 street_wv2 train_wv1 train_wv2"
    for test in $testsets; do
        ./decode_with_ivec.sh --nj 8 --num-threads 4 --acwt 0.10 --config conf/decode_dnn.config \
            --ivector-dim $ivector_dim --ivector-scp test_ivector/test_spk_ivector_${test}_${dec_env}.scp --ivector-map test_utt2spk/test_utt2spk_${test} --ivector-scale $ivector_scale --splice $splice \
            exp/tri3a_dnn2048_${nn_name}_cmn_delta_with_ivec/graph_${lm_data} data-fbank/$test $dir/decode_${lm_data}_${test} || exit 1;
    done
}

function decode {
    [[ -z "$dec_ivector_scp" ]] && exit 1
    [[ -z "$dec_ivector_map" ]] && exit 1
    utils/mkgraph.sh data/lang_test_${lm_data} exp/tri3a_dnn2048_${nn_name}_cmn_delta_with_ivec exp/tri3a_dnn2048_${nn_name}_cmn_delta_with_ivec/graph_${lm_data} || exit 1;
    dir=exp/tri3a_dnn2048_${nn_name}_cmn_delta_with_ivec
    testsets="airport_wv1 airport_wv2 babble_wv1 babble_wv2 car_wv1 car_wv2 clean_wv1 clean_wv2 restaurant_wv1 restaurant_wv2 street_wv1 street_wv2 train_wv1 train_wv2"
    for test in $testsets; do
        ./decode_with_ivec.sh --nj 8 --num-threads 4 --acwt 0.10 --config conf/decode_dnn.config \
            --ivector-dim $ivector_dim --ivector-scp ${dec_ivector_scp} --ivector-map ${dec_ivector_map} --ivector-scale $ivector_scale --splice $splice \
            exp/tri3a_dnn2048_${nn_name}_cmn_delta_with_ivec/graph_${lm_data} data-fbank/$test $dir/decode_combined_${lm_data}_${test} || exit 1;
    done
}
