#!/bin/bash
ivector_scp=train_ivector/spk_ivector.scp
ivector_map=train_utt2spk/utt2spk
nn_name=clean
tr_env=clean
dec_env=clean
. run_with_ivec.sh

rbm_pretrain
nn_train
multi_env_decode
