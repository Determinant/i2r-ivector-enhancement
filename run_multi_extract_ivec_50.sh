#!/bin/bash
ubm_name=multi
train_set=train_si84_multi
dev_set=dev_0330
ivector_dim=50
testsets="airport_wv1 airport_wv2 babble_wv1 babble_wv2 car_wv1 car_wv2 clean_wv1 clean_wv2 restaurant_wv1 restaurant_wv2 street_wv1 street_wv2 train_wv1 train_wv2"

. run_extract_ivec.sh
true && train_ivector_extractor
true && extract_ivectors "$train_set" "$train_set"
true && extract_ivectors "$dev_set" "${dev_set}_${ubm_name}" 4

true && {
    for test in $testsets; do
        extract_ivectors "$test" "${test}_${ubm_name}" 3
    done
}

true && for test in $testsets; do
    cat exp/ivectors_${test}_${ubm_name}_{,fe}male_$ivector_dim/spk_ivector.scp > test_ivector/test_spk_ivector_${test}_${ubm_name}_${ivector_dim}.scp
done

cat exp/ivectors_{${train_set},${dev_set}_${ubm_name}}_{,fe}male_$ivector_dim/spk_ivector.scp > train_ivector/spk_ivector_${ubm_name}_$ivector_dim.scp
