#include "base/kaldi-common.h"
#include "util/table-types.h"
#include "util/parse-options.h"
#include <map>

using namespace kaldi;
const char *usage =
    "Copy vectors to single-row matrices (e.g. speaker vector)\n"
    "Usage: append-ivectors <in-rspecifier> <out-wspecifier>\n\n";
typedef std::pair<std::string, std::string> StringPair_t;
typedef std::map<std::string, std::string> StringToString_t;
struct Buffer {
    Matrix<BaseFloat> mat;
    int count;
    std::string utter;
    std::vector<std::string> recover;
};
typedef std::map<StringPair_t, Buffer> Agg_t;
std::string recover_map;
StringToString_t utt2spkr;
StringToString_t utt2noise;
Agg_t agg;
int fpu = 256;

int main(int argc, char *argv[]) {
    try {
        ParseOptions po(usage);
        std::string utt2spk_file, utt2noise_file;

        po.Register("recover_map", &recover_map, "recover mapping file");
        po.Register("utt2spk", &utt2spk_file, "utt2spk mapping");
        po.Register("utt2noise", &utt2noise_file, "utt2noise mapping");
        po.Register("fpu", &fpu, "frames per utterance");
        po.Read(argc, argv);

        if (po.NumArgs() != 2)
        {
            po.PrintUsage();
            exit(1);
        }
        std::string feature_rspecifier = po.GetArg(1),
                    feature_wspecifier = po.GetArg(2);

        SequentialTokenVectorReader utt2spkr_reader = SequentialTokenVectorReader(utt2spk_file);
        SequentialTokenVectorReader utt2noise_reader = SequentialTokenVectorReader(utt2noise_file);
        TokenVectorWriter recover_writer = TokenVectorWriter(recover_map);
        SequentialBaseFloatVectorReader feature_reader = SequentialBaseFloatVectorReader(feature_rspecifier);
        BaseFloatMatrixWriter feature_writer = BaseFloatMatrixWriter(feature_wspecifier);

        for (; !utt2spkr_reader.Done(); utt2spkr_reader.Next())
        {
            const std::vector<std::string> spkr = utt2spkr_reader.Value();
            assert(spkr.size() >= 1);
            fprintf(stderr, "%s => %s\n", utt2spkr_reader.Key().c_str(), (*spkr.begin()).c_str());
            utt2spkr[utt2spkr_reader.Key()] = *spkr.begin();
        }
        for (; !utt2noise_reader.Done(); utt2noise_reader.Next())
        {
            const std::vector<std::string> noise = utt2noise_reader.Value();
            assert(noise.size() >= 1);
            fprintf(stderr, "%s => %s\n", utt2noise_reader.Key().c_str(), (*noise.begin()).c_str());
            utt2noise[utt2noise_reader.Key()] = *noise.begin();
        }

        for (; !feature_reader.Done(); feature_reader.Next())
        {
            const std::string &utter = feature_reader.Key();
            const Vector<BaseFloat> &vfeat = feature_reader.Value();
            StringToString_t::iterator its = utt2spkr.find(utter);
            if (its == utt2spkr.end())
            {
                fprintf(stderr, "spkr for %s not found\n", utter.c_str());
                exit(-1);
            }
            StringToString_t::iterator itn = utt2noise.find(utter);
            if (itn == utt2noise.end())
            {
                fprintf(stderr, "noise for %s not found\n", utter.c_str());
                exit(-1);
            }
            StringPair_t key = StringPair_t(its->second, itn->second);
            Agg_t::iterator ita = agg.find(key);
            if (ita == agg.end())
            {
                Buffer &p = agg[key];
                p.mat = Matrix<BaseFloat>(fpu, vfeat.Dim());
                p.count = 0;
                p.utter = utter;
                p.recover.clear();
                p.recover.push_back(key.first);
                p.recover.push_back(key.second);
            }
            Buffer &p = agg[key];
            if (p.count == fpu)
            {
                feature_writer.Write(p.utter, p.mat);
                recover_writer.Write(p.utter, p.recover);
                p.count = 0;
                p.utter = utter;
                p.recover.clear();
                p.recover.push_back(key.first);
                p.recover.push_back(key.second);
            }
            p.recover.push_back(utter);
            memmove(p.mat.RowData(p.count++), vfeat.Data(), sizeof(BaseFloat) * vfeat.Dim());
        }
        for (Agg_t::iterator it = agg.begin(); it != agg.end(); it++)
        {
            Buffer &p = it->second;
            if (p.count > 0)
            {
                int m = p.mat.NumCols();
                Matrix<BaseFloat> mat(p.count, m);
                for (int i = 0; i < p.count; i++)
                    memmove(mat.RowData(i), p.mat.RowData(i), sizeof(BaseFloat) * m);
                feature_writer.Write(p.utter, mat);
                recover_writer.Write(p.utter, p.recover);
            }
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what();
        return -1;
    }
    return 0;
}
