#!/bin/bash
ivector_scp=train_ivector/spk_ivector_clean-mi.scp
ivector_map=train_utt2spk/utt2spk
nn_name=clean-mi
tr_env=clean
dec_env=multi
. run_with_ivec.sh

rbm_pretrain
nn_train
multi_env_decode
