#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cstring>

double *coef;
int dim;
char token[1024], opt[1024];

int main(int argc, char **argv) {
    assert(argc == 2);
    dim = atoi(argv[1]);
    printf("<Nnet>\n");
    while (scanf("%s", opt) != EOF)
    {
        int n;
        bool aflag = strcmp(opt, "<AddShift>") == 0;
        bool rflag = strcmp(opt, "<Rescale>") == 0;
        if (aflag || rflag)
        {
            scanf("%d %d %s %s %s", &n, &n, token, token, token);
            coef = new double[n + dim];
            for (int i = 0; i < n; i++)
                scanf("%lf", coef + i);
            for (int i = 0; i < dim; i++)
                coef[n + i] = aflag ? 0.0 : 1.0;
            n += dim;
            printf("%s %d %d\n<LearnRateCoef> 0 [ ", opt, n, n);
            for (int i = 0; i < n; i++)
                printf("%.8f ", coef[i]);
            printf("]\n");
        }
    }
    printf("</Nnet>\n");
    return 0;
}
