#include "base/kaldi-common.h"
#include "util/table-types.h"
#include "util/parse-options.h"
#include <map>

using namespace kaldi;
const char *usage =
    "Copy vectors to single-row matrices (e.g. speaker vector)\n"
    "Usage: append-ivectors <in-rspecifier> <out-wspecifier>\n\n";

int main(int argc, char *argv[]) {
    try {
        ParseOptions po(usage);
        po.Read(argc, argv);
        if (po.NumArgs() != 2)
        {
            po.PrintUsage();
            exit(1);
        }
        std::string feature_rspecifier = po.GetArg(1),
                    feature_wspecifier = po.GetArg(2);

        SequentialBaseFloatVectorReader feature_reader = SequentialBaseFloatVectorReader(feature_rspecifier);
        BaseFloatMatrixWriter feature_writer = BaseFloatMatrixWriter(feature_wspecifier);

        for (; !feature_reader.Done(); feature_reader.Next())
        {
            const std::string &utter = feature_reader.Key();
            const Vector<BaseFloat> &vfeat = feature_reader.Value();
            Matrix<BaseFloat> mfeat(1, vfeat.Dim());
            memmove(mfeat.RowData(0), vfeat.Data(), sizeof(BaseFloat) * vfeat.Dim());
            feature_writer.Write(utter, mfeat);
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what();
        return -1;
    }
    return 0;
}
