#include "base/kaldi-common.h"
#include "util/table-types.h"
#include "util/parse-options.h"
#include <map>

using namespace kaldi;
const char *usage =
    "Concatenate features with speaker level ivector (e.g. appending speaker\n"
    "ivector to each frame of the corresponding utterance)\n"
    "Usage: append-ivectors <ivector-rspecifier> <utt2spk-rspecifier> <in-rspecifier2> <out-wspecifier>\n\n"
    "e.g.: append-ivectors 'ark:copy-vector scp:spk_ivector.scp ark:- |' "
    "ark:aurora4/s5/data/train_si84_clean/utt2spk 'ark:copy-feats scp:t.scp ark:- |' 'ark,t:-'\n";

typedef std::map<std::string, std::string> StringToString_t;
typedef std::map<std::string, Vector<BaseFloat> > StringToVector_t;
StringToString_t utt2spkr;
StringToVector_t spkr_ivectors;
BaseFloat scale = 1.0;

int main(int argc, char *argv[]) {
    try {
        ParseOptions po(usage);
        po.Register("scale", &scale, "scaling factor of ivectors");
        po.Read(argc, argv);
        if (po.NumArgs() != 4)
        {
            po.PrintUsage();
            exit(1);
        }
        std::string spkr_ivector_rspecifier = po.GetArg(1),
                    utt2spkr_rspecifier = po.GetArg(2),
                    feature_rspecifier = po.GetArg(3),
                    feature_wspecifier = po.GetArg(4);

        SequentialBaseFloatVectorReader spkr_ivector_reader = SequentialBaseFloatVectorReader(spkr_ivector_rspecifier);
        SequentialTokenVectorReader utt2spkr_reader = SequentialTokenVectorReader(utt2spkr_rspecifier);
        SequentialBaseFloatMatrixReader feature_reader = SequentialBaseFloatMatrixReader(feature_rspecifier);
        BaseFloatMatrixWriter feature_writer = BaseFloatMatrixWriter(feature_wspecifier);

        for (; !spkr_ivector_reader.Done(); spkr_ivector_reader.Next())
        {
            fprintf(stderr, "read ivector for spkr: %s\n", spkr_ivector_reader.Key().c_str());
            spkr_ivectors[spkr_ivector_reader.Key()] = spkr_ivector_reader.Value();
        }
        for (; !utt2spkr_reader.Done(); utt2spkr_reader.Next())
        {
            const std::vector<std::string> spkr = utt2spkr_reader.Value();
            assert(spkr.size() >= 1);
            fprintf(stderr, "%s => %s\n", utt2spkr_reader.Key().c_str(), (*spkr.begin()).c_str());
            utt2spkr[utt2spkr_reader.Key()] = *spkr.begin();
        }

        for (; !feature_reader.Done(); feature_reader.Next())
        {
            const std::string &utter = feature_reader.Key();
            StringToString_t::iterator it = utt2spkr.find(utter);
            if (it == utt2spkr.end())
            {
                fprintf(stderr, "spkr for %s not found\n", utter.c_str());
                exit(-1);
            }
            StringToVector_t::iterator it2 = spkr_ivectors.find(it->second);
            if (it2 == spkr_ivectors.end())
            {
                fprintf(stderr, "ivector for spkr %s not found\n", it->second.c_str());
                exit(-1);
            }
            const Vector<BaseFloat> &ivector = it2->second;
            const Matrix<BaseFloat> &feat = feature_reader.Value();
            int n = feat.NumRows();
            int m = feat.NumCols();
            int im = ivector.Dim();
            Matrix<BaseFloat> appended(n, m + im);
            for (int i = 0; i < n; i++)
            {
                memmove(appended.RowData(i), feat.RowData(i), sizeof(BaseFloat) * m);
                memmove(appended.RowData(i) + m, ivector.Data(), sizeof(BaseFloat) * im);
                BaseFloat *row = appended.RowData(i) + m;
                if (scale != 1.0)
                    for (int j = 0; j < im; j++) row[j] *= scale;
            }
            feature_writer.Write(utter, appended);
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what();
        return -1;
    }
    return 0;
}
