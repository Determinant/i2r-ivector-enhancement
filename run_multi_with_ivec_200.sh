#!/bin/bash
ivector_scp=train_ivector/spk_ivector_multi_200.scp
ivector_map=train_utt2spk/utt2spk_multi
ivector_dim=200
nn_name=multi_200
tr_env=multi
dec_env=multi_200
. run_with_ivec.sh

rbm_pretrain
nn_train
multi_env_decode
