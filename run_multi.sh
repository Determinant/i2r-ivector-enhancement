#!/bin/bash

. ./cmd.sh ## You'll want to change cmd.sh to something that will work on your system.
           ## This relates to the queue.

. ./path.sh
# This is a shell script, but it's recommended that you run the commands one by
# one by copying and pasting into the shell.


aurora4=/home/share/aurora4/s5/corpus/aurora4
#we need lm, trans, from WSJ0 CORPUS
wsj0=/home/share/aurora4//s5/corpus/wsj0
lm_data=htk_lm

#local/aurora4_data_prep.sh $aurora4 $wsj0
#
#local/wsj_prepare_dict.sh || exit 1;
#
#utils/prepare_lang.sh data/local/dict "<SPOKEN_NOISE>" data/local/lang_tmp data/lang || exit 1;
#
#local/aurora4_format_data.sh || exit 1;

# Now make MFCC features.
# mfccdir should be some place with a largish disk where you
# want to store MFCC features.
mfccdir=mfcc
false && {
for x in train_si84_multi train_si84_multi test_eval92 dev_0330 dev_1206; do 
 steps/make_mfcc.sh  --nj 10 \
   data/$x exp/make_mfcc/$x $mfccdir || exit 1;
 steps/compute_cmvn_stats.sh data/$x exp/make_mfcc/$x $mfccdir || exit 1;
done
}
# make fbank features
fbankdir=fbank
true && {
mkdir -p data-fbank
#for x in train_si84_multi train_si84_multi dev_0330 dev_1206 test_eval92; do
testsets="clean_wv1 clean_wv2 airport_wv1 airport_wv2 babble_wv1 babble_wv2 car_wv1 car_wv2 restaurant_wv1 restaurant_wv2 street_wv1 street_wv2 train_wv1 train_wv2"
for x in $testsets; do
  cp -r data/$x data-fbank/$x
  steps/make_fbank.sh --nj 10 \
    data-fbank/$x exp/make_fbank/$x $fbankdir || exit 1;
 steps/compute_cmvn_stats.sh data-fbank/$x exp/make_fbank/$x $fbankdir || exit 1;
done
}
false && {
# Note: the --boost-silence option should probably be omitted by default
# for normal setups.  It doesn't always help. [it's to discourage non-silence
# models from modeling silence.]

steps/train_mono.sh --boost-silence 1.25 --nj 10  \
  data/train_si84_multi data/lang exp/mono0a_multi || exit 1;

steps/align_si.sh --boost-silence 1.25 --nj 10  \
   data/train_si84_multi data/lang exp/mono0a_multi exp/mono0a_multi_ali || exit 1;


steps/train_deltas.sh --boost-silence 1.25 \
    2000 10000 data/train_si84_multi data/lang exp/mono0a_multi_ali exp/tri1_multi || exit 1;


steps/align_si.sh --nj 10 \
  data/train_si84_multi data/lang exp/tri1_multi exp/tri1_multi_ali_si84 || exit 1;

steps/train_deltas.sh  \
  2500 15000 data/train_si84_multi data/lang exp/tri1_multi_ali_si84 exp/tri2a_multi || exit 1;


steps/train_lda_mllt.sh \
   --splice-opts "--left-context=3 --right-context=3" \
   2500 15000 data/train_si84_multi data/lang exp/tri1_multi_ali_si84 exp/tri2b_multi || exit 1;


utils/mkgraph.sh data/lang_test_${lm_data} exp/tri2b_multi exp/tri2b_multi/graph_${lm_data} || exit 1;
steps/decode.sh --nj 8 \
  exp/tri2b_multi/graph_${lm_data} data/test_eval92 exp/tri2b_multi/decode_${lm_data}_eval92 || exit 1;

# Align tri2b system with si84 multi-condition data.
steps/align_si.sh  --nj 10 \
  --use-graphs true data/train_si84_multi data/lang exp/tri2b_multi exp/tri2b_multi_ali_si84  || exit 1;

steps/align_si.sh  --nj 10 \
  data/dev_0330 data/lang exp/tri2b_multi exp/tri2b_multi_ali_dev_0330 || exit 1;

steps/align_si.sh  --nj 10 \
  data/dev_1206 data/lang exp/tri2b_multi exp/tri2b_multi_ali_dev_1206 || exit 1;
}

false && local/run_mmi_tri2b_multi.sh

false && {

# Align tri2b_mmi system with si84 multi-condition data.
steps/align_si.sh  --nj 10 \
  --use-graphs false data/train_si84_multi data/lang exp/tri2b_multi_mmi_b0.1 exp/tri2b_multi_mmi_b0.1_ali_si84  || exit 1;

steps/align_si.sh  --nj 10 \
  data/dev_0330 data/lang exp/tri2b_multi_mmi_b0.1 exp/tri2b_multi_mmi_b0.1_ali_dev_0330 || exit 1;

steps/align_si.sh  --nj 10 \
  data/dev_1206 data/lang exp/tri2b_multi_mmi_b0.1 exp/tri2b_multi_mmi_b0.1_ali_dev_1206 || exit 1;
}

false && {
#Now begin train DNN systems on multi data
#RBM pretrain
    dir=exp/tri3a_dnn2048_multi_cmn_delta_pretrain
    $cuda_cmd $dir/_pretrain_dbn.log \
      steps/nnet/pretrain_dbn.sh  --apply-cmvn "true" --norm-vars "false" --delta-order 2 \
        --hid-dim 2048 --nn-depth 7 --rbm-iter 5 data-fbank/train_si84_multi $dir
}

false && {
dir=exp/tri3a_dnn2048_multi_cmn_delta
ali=exp/tri2b_multi_mmi_b0.1_ali_si84
ali_dev=exp/tri2b_multi_mmi_b0.1_ali_dev_0330
feature_transform=exp/tri3a_dnn2048_multi_cmn_delta_pretrain/final.feature_transform
dbn=exp/tri3a_dnn2048_multi_cmn_delta_pretrain/7.dbn
$cuda_cmd $dir/_train_nnet.log \
  steps/nnet/train.sh --apply-cmvn "true" --norm-vars "false" --delta-order 2 --randomizer-size 200000 \
    --feature-transform $feature_transform --dbn $dbn --hid-layers 0 --learn-rate 0.008 \
      data-fbank/train_si84_multi data-fbank/dev_0330 data/lang $ali $ali_dev $dir || exit 1;

##Now begin train DNN systems on multi data
##RBM pretrain
#dir=exp/tri3a_dnn2048_multi_pretrain
#$cuda_cmd $dir/_pretrain_dbn.log \
#  steps/nnet/pretrain_dbn.sh  --hid-dim 1024 --nn-depth 7 --rbm-iter 3 data-fbank/train_si84_multi $dir
#
#dir=exp/tri3a_dnn2048_multi
#ali=exp/tri2b_multi_mmi_b0.1_ali_si84
#ali_dev=exp/tri2b_multi_mmi_b0.1_ali_dev_0330
#feature_transform=exp/tri3a_dnn2048_multi_pretrain/final.feature_transform
#dbn=exp/tri3a_dnn2048_multi_pretrain/7.dbn
#$cuda_cmd $dir/_train_nnet.log \
#  steps/nnet/train.sh --feature-transform $feature_transform --dbn $dbn --hid-layers 0 --learn-rate 0.008 \
#  data-fbank/train_si84_multi data-fbank/dev_0330 data/lang $ali $ali_dev $dir || exit 1;
}

false && {
utils/mkgraph.sh data/lang_test_${lm_data} exp/tri3a_dnn2048_multi_cmn_delta exp/tri3a_dnn2048_multi_cmn_delta/graph_${lm_data} || exit 1;
dir=exp/tri3a_dnn2048_multi_cmn_delta
#testsets="car_wv2"
testsets="clean_wv1 clean_wv2 airport_wv1 airport_wv2 babble_wv1 babble_wv2 car_wv1 car_wv2 restaurant_wv1 restaurant_wv2 street_wv1 street_wv2 train_wv1 train_wv2"
for test in $testsets; do
steps/nnet/decode.sh --nj 8 --num-threads 4 --acwt 0.10 --config conf/decode_dnn.config \
  exp/tri3a_dnn2048_multi_cmn_delta/graph_${lm_data} data-fbank/$test $dir/decode_${lm_data}_${test} || exit 1;
done

#utils/mkgraph.sh data/lang_test_${lm_data} exp/tri3a_dnn2048_multi exp/tri3a_dnn2048_multi/graph_${lm_data} || exit 1;
#dir=exp/tri3a_dnn2048_multi
#testsets="airport_wv1 airport_wv2 babble_wv1 babble_wv2 car_wv1 car_wv2 multi_wv1 multi_wv2 restaurant_wv1 restaurant_wv2 street_wv1 street_wv2 train_wv1 train_wv2"
#for test in $testsets; do
#steps/nnet/decode.sh --nj 8 --num-threads 4 --acwt 0.10 --config conf/decode_dnn.config \
#  exp/tri3a_dnn2048_multi/graph_${lm_data} data-fbank/$test $dir/decode_${lm_data}_${test} || exit 1;
#done
}
